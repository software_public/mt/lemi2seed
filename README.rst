==========
lemi2seed
==========

- **Description**:
    - Convert LEMI data and metadata into day-long mseed and StationXML files.

- **Usage**:
    - ``lemi2seed -h``
    - ``lemi2seed -d path2data -qc`` *(run lemi2seed in "quality control" mode)*
    - ``lemi2seed -d path2data -g`` *(run lemi2seed in "gui only" mode)*
    - ``lemi2seed -d path2data -g -s path2savedmd`` *(run lemi2seed in "gui only" mode after loading previously saved metadata fields)*
    - ``lemi2seed -d path2data -m path2metadata`` *(run lemi2seed in "field sheet and gui" mode)*

  The "quality control" mode allows to quickly convert LEMI data to day-long mseed files. The GUI and handling of metadata are bypassed.

  In the "gui only" mode, metadata are entered/modified using the GUI only.

  In the "field sheet and gui" mode, metadata are prepopulated from parsing the user-provided field sheets and modified using the GUI.

- **License**: GNU General Public License v3 (GPLv3)
