# -*- coding: utf-8 -*-

"""Top-level package for lemi2seed."""

__author__ = """EPIC"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2023.2.0.0'
