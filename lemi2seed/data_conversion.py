#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Routines to:
- convert LEMI data to day-long mseed files
- save LEMI calibration coefficients to a log file

Maeva Pourpoint - IRIS/PASSCAL
"""

from __future__ import annotations

import numpy as np
import time
import typing

from obspy import read, Stream, Trace
from obspy.core.trace import Stats
from pathlib import Path
from typing import List, TYPE_CHECKING

from lemi2seed.logging import parse_config_ini, setup_logger

if TYPE_CHECKING:
    from lemi2seed.lemi_data import LemiData

# Read config.ini file and set up logging
config = parse_config_ini()
logger = setup_logger(__name__)


@typing.no_type_check
def update_stats(datum: np.void, stats: Stats) -> Stats:
    """
    Update Stats object with proper channel name, location code and start time
    of the trace.
    """
    stats['channel'] = datum['cha_name']
    stats['location'] = datum['loc']
    stats['starttime'] = datum['starttime']
    return stats


def init_stats(lemi_data: LemiData) -> Stats:
    """
    Instantiate Stats object with proper network code, station name and
    sampling rate.
    """
    stats = Stats()
    stats['network'] = lemi_data.stats['net']
    stats['station'] = lemi_data.stats['sta']
    stats['sampling_rate'] = lemi_data.stats['sample_rate']
    return stats


def handle_existing_mseed_file(st_current: Stream, filename: Path) -> Stream:
    """
    If for a given day a mseed file already exists, read existing mseed file
    into a Stream object and merge that Stream object to the current Stream
    object.
    """
    st_existing = read(str(filename))
    st_new = st_existing + st_current
    return st_new


def write_daylong_mseed(lemi_data: LemiData) -> None:
    """Create daylong mseed files for each channel and location code."""
    path2mseed = lemi_data.output_mseed.joinpath(lemi_data.stats['sta'])
    if not path2mseed.is_dir():
        path2mseed.mkdir(parents=True)
    stats = init_stats(lemi_data)
    chas = np.unique(lemi_data.data_np["cha_name"])
    locs = np.unique(lemi_data.data_np["loc"])
    for cha in chas:
        logger.info("Writing day-long mseed files for channel: {}.".format(cha))
        for loc in locs:
            ind_data = np.logical_and(lemi_data.data_np['cha_name'] == cha,
                                      lemi_data.data_np['loc'] == loc)
            data = lemi_data.data_np[ind_data]
            dates = set([(datum['starttime'].julday, datum['starttime'].year) for datum in data])
            for day, year in sorted(dates):
                data_day = [x for x in data if x['starttime'].julday == day]
                st_day = Stream()
                for datum in data_day:
                    tr_day = Trace()
                    tr_day.stats = update_stats(datum, stats)
                    tr_day.data = datum['samples'][:datum['npts']]
                    st_day.append(tr_day.copy())
                filename = path2mseed.joinpath('.'.join([tr_day.stats.station, tr_day.stats.network,
                                                         loc, cha, str(year), str(day).zfill(3)]))
                if filename.is_file():
                    logger.info("The mseed file {} already exists. Merging to "
                                "it. There will be no duplication of existing data."
                                .format(filename))
                    st_day = handle_existing_mseed_file(st_day, filename)
                st_day.merge(-1)
                logger.info("Writing day-long mseed file: {}.".format(filename))
                st_day.write(filename, format='MSEED')
    logger.info("Writing of day-long mseed files successfully completed!")


def get_log_filename(lemi_data: LemiData) -> Path:
    """
    Create log file name for given station deployment based on the start and
    end time of that deployment.
    """
    path2log = lemi_data.output_log.joinpath(lemi_data.stats['sta'])
    if not path2log.is_dir():
        path2log.mkdir(parents=True)
    starttime = lemi_data.data_np['starttime'][0].strftime('%Y%j_%H%M%S')
    endtime = lemi_data.data_np['endtime'][-1].strftime('%Y%j_%H%M%S')
    log_file = path2log.joinpath(f"LEMI424_{starttime}_{endtime}.log")
    return log_file


def parse_def_files(def_files: List[Path]) -> List[str]:
    """
    Parse calibration coefficient files and return reformatted calibration
    information.
    """
    def_info: List[str] = []
    for def_file in def_files:
        try:
            with open(def_file, 'r') as fin:
                lines = fin.readlines()
        except OSError as error:
            logger.error("Failed to open def file: {} - Skipping file "
                         "- Exception: {}".format(def_file, error))
        else:
            def_info = [*def_info, *lines, '\n--------------------------\n\n']
    return def_info


def write_log_file(lemi_data: LemiData) -> None:
    """
    Create log file with calibration coefficient information from all generated
    def files.
    """
    log_file = get_log_filename(lemi_data)
    logger.info("Writing calibration coefficients in log file: {}.".format(log_file))
    def_info = parse_def_files(lemi_data.def_files)
    with open(log_file, 'w') as fout:
        fout.write(f"{config['software']['name']}: v{config['software']['version']} - "
                   f"Run time (UTC): {time.ctime(time.time())}\n\n")
        fout.write(''.join(def_info))
    logger.info("Writing of calibration coefficients in log file successfully "
                "completed!")
