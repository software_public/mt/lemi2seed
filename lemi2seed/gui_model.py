# -*- coding: utf-8 -*-
"""
lemi2seed GUI Framework - Model.

Maeva Pourpoint - IRIS/PASSCAL
"""

from obspy import UTCDateTime
from PySide6 import QtCore, QtGui

from lemi2seed.logging import setup_logger

# Set up logging
logger = setup_logger(__name__)


class TreeItem:

    def __init__(self, data, parent=None):
        self.parent_item = parent
        self.item_data = data
        self.child_items = []

    def child(self, row):
        if row >= 0:
            return self.child_items[row]
        return None

    def child_count(self):
        return len(self.child_items)

    def child_number(self):
        if self.parent_item is not None:
            return self.parent_item.child_items.index(self)
        return 0

    def column_count(self):
        return len(self.item_data)

    def data(self, column):
        try:
            return self.item_data[column]
        except IndexError:
            return None

    def get_child_index(self, column, val):
        child_vals = [child_item.data(column) for child_item in self.child_items]
        return child_vals.index(val)

    def insert_children(self, position, count, columns):
        if position < 0 or position > len(self.child_items):
            return False
        for row in range(count):
            data = [None for _ in range(columns)]
            item = TreeItem(data, self)
            self.child_items.insert(position, item)
        return True

    def parent(self):
        return self.parent_item

    def remove_children(self, position, count):
        if position < 0 or position + count > len(self.child_items):
            return False
        for row in range(count):
            self.child_items.pop(position)
        return True

    def setData(self, column, val):
        if column < 0 or column >= len(self.item_data):
            return False
        self.item_data[column] = val
        return True


class TreeModel(QtCore.QAbstractItemModel):

    def __init__(self, headers, data, parent=None):
        super().__init__(parent)
        root_data = list(headers)
        self.root_item = TreeItem(root_data)
        self.input_data = data
        self.category_index = {}
        self.setup_model_data(data, self.root_item)

    def columnCount(self, parent=QtCore.QModelIndex()):
        if parent.isValid():
            return parent.internalPointer().column_count()
        else:
            return self.root_item.column_count()

    def data(self, index, role):
        if not index.isValid():
            return None
        if role not in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            return None
        item = self.get_item(index)
        return item.data(index.column())

    @staticmethod
    def flags(index):
        if not index.isValid():
            return 0
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def get_index_for_item(self, item):
        return self.createIndex(item.child_number(), 0, item)

    def get_item(self, index):
        if index.isValid():
            item = index.internalPointer()
            if item:
                return item
        return self.root_item

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.root_item.data(section)
        return None

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if parent.isValid() and parent.column() != 0:
            return QtCore.QModelIndex()
        parent_item = self.get_item(parent)
        child_item = parent_item.child(row)
        if child_item:
            return self.createIndex(row, column, child_item)
        else:
            return QtCore.QModelIndex()

    def insertRows(self, position, rows, parent=QtCore.QModelIndex()):
        parent_item = self.get_item(parent)
        self.beginInsertRows(parent, position, position + rows - 1)
        success = parent_item.insert_children(position, rows, self.root_item.column_count())
        self.endInsertRows()
        return success

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()
        child_item = self.get_item(index)
        parent_item = child_item.parent()
        if parent_item == self.root_item:
            return QtCore.QModelIndex()
        return self.createIndex(parent_item.child_number(), 0, parent_item)

    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        parent_item = self.get_item(parent)
        self.beginRemoveRows(parent, position, position + rows - 1)
        success = parent_item.remove_children(position, rows)
        self.endRemoveRows()
        return success

    def rowCount(self, parent=QtCore.QModelIndex()):
        parent_item = self.get_item(parent)
        return parent_item.child_count()

    def setData(self, index, val, role=QtCore.Qt.EditRole):
        """
        Set tree item at given index to given input value. If the given value
        is different from the existing value at that index, update the input
        data.
        """
        if role == QtCore.Qt.EditRole:
            try:
                item = self.get_item(index)
                old_val = item.item_data[1]
                result = item.setData(index.column(), val)
            except Exception as e:
                logger.error("Failed to set data field (error: {})!".format(e))
            else:
                if result:
                    self.dataChanged.emit(index, index)
                    if old_val != val:
                        self.update_input_data(item.parent(), item.item_data)
                return result
        return False

    def setHeaderData(self, section, orientation, val, role=QtCore.Qt.EditRole):
        """
        Set tree item for given role and section in the header with the
        specified orientation to the provided value.
        """
        if role != QtCore.Qt.EditRole or orientation != QtCore.Qt.Horizontal:
            return False
        result = self.root_item.setData(section, val)
        if result:
            self.headerDataChanged.emit(orientation, section, section)
        return result

    def setup_model_data(self, data, parent):
        """
        Recursively fill tree model by traversing the nested dictionnary input
        data.
        """
        for key, val in data.items():
            parent.insert_children(parent.child_count(), 1, self.root_item.column_count())
            child_item = parent.child(parent.child_count() - 1)
            if isinstance(val, dict):
                child_item.setData(0, key)
                self.category_index.update({key: self.get_index_for_item(child_item)})
                self.setup_model_data(val, child_item)
            else:
                child_item.setData(0, key)
                if isinstance(val, UTCDateTime):
                    val = QtCore.QDateTime.fromString(str(val), QtCore.Qt.ISODate)
                child_item.setData(1, val)

    def update_input_data(self, parent_item, new_item_data):
        """
        Update data used to set the model with user provided inputs. Certain
        inputs require to be reformatted before updating the data items.
        """
        parent_key = [parent_item.item_data[0]]
        while parent_item.parent() is not self.root_item:
            parent_item = parent_item.parent()
            parent_key = [parent_item.item_data[0], *parent_key]
        key, val = new_item_data
        keys = [*parent_key, key]
        if isinstance(val, str):
            val = QtGui.QTextDocumentFragment.fromHtml(val).toPlainText()
            if val == '':
                val = None
        if isinstance(val, QtCore.QDateTime):
            val = UTCDateTime(val.toString("yyyy-MM-ddTHH:mm:ss"))
        self.input_data = self.update_dict(self.input_data, self.nested_dict(keys, val))

    def nested_dict(self, keys, val):
        """Create nested dictionnary using recursion."""
        if keys:
            head, *tail = keys
            return {head: self.nested_dict(tail, val)}
        else:
            return val

    def update_dict(self, dictionary, update):
        """
        Update nested dictionary with user provided inputs using recursion.
        """
        for key, val in update.items():
            if isinstance(val, dict):
                dictionary[key] = self.update_dict(dictionary.get(key, {}), val)
            else:
                dictionary[key] = val
        return dictionary
