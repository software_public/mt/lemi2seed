# -*- coding: utf-8 -*-
"""
lemi2seed GUI Framework -  View.

Maeva Pourpoint - IRIS/PASSCAL
"""

from dataclasses import fields
from operator import attrgetter, methodcaller
from pathlib import Path
from PySide6 import QtCore, QtWidgets, QtGui

from lemi2seed.data_conversion import write_daylong_mseed, write_log_file
from lemi2seed.gui_model import TreeModel
from lemi2seed.gui_widgets import (load_ui, BaseWidget, NetWidgets,
                                   StaWidgets, RunWidgets, HelpWidgets,
                                   get_channel_widgets)
from lemi2seed.lemi_metadata import LemiMetadata
from lemi2seed.logging import parse_config_ini, setup_logger
from lemi2seed.metadata_category import MSG_MAP
from lemi2seed.metadata_conversion import write_stationxml
from lemi2seed.utils import get_e_ids, get_run_list, NUM_E_CHA_MAX

# Read config.ini file and set up logging
config = parse_config_ini()
logger = setup_logger(__name__)

VALID_NUM_E_PAIRS = [0, 2, 4]


class LemiWindow(*load_ui('mainwindow.ui')):

    def __init__(self, lemi_data, lemi_md, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(f"{config['software']['name']} - {config['software']['version']}")
        self.lemi_data = lemi_data
        self.lemi_md = lemi_md
        # -- Tool bar --
        self.help_window = HelpWidgets()
        self.actionHelp.triggered.connect(self.toggle_help_window)
        self.actionLoad_MD.triggered.connect(self.load_md)
        self.actionSave_MD.triggered.connect(self.save_md)
        # -- Network --
        self.net_widgets = NetWidgets(self)
        self.uiNetEditor.addWidget(self.net_widgets)
        # -- Station --
        self.sta_widgets = StaWidgets(self)
        self.uiStaEditor.addWidget(self.sta_widgets)
        # -- Runs --
        self.num_runs = lemi_md.data_stats["num_runs"]
        self.run_list = get_run_list(self.num_runs)
        self.set_run_editor_forms()
        # -- Electric --
        ElecWidgets = get_channel_widgets([BaseWidget, *load_ui("electricwidget.ui")])
        self.set_elec_editor_forms(ElecWidgets)
        # -- Magnetic --
        MagWidgets = get_channel_widgets([BaseWidget, *load_ui("magneticwidget.ui")])
        self.set_mag_editor_forms(MagWidgets)
        self.uiMagEditor.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        # -- Set Model, Process metadata, Convert data and metadata to archivable format --
        self.set_model()
        self.processing_conversion_steps()

    def toggle_help_window(self):
        if self.help_window.isVisible():
            self.help_window.hide()
        else:
            self.help_window.show()

    def create_message_box(self, text, details, icon):
        """
        Create generic message box. This box will be used to display different
        texts to alert the user to a given situation.
        """
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(13)
        msg_box = QtWidgets.QMessageBox(self)
        msg_box.setFont(font)
        msg_box.setText(text)
        msg_box.setInformativeText(details)
        if icon:
            msg_box.setIcon(icon)
        return msg_box

    def reset_model(self, filename):
        """
        Reset current metadata model.
        Called when loading a previously saved metadata model.
        """
        lemi_md_saved = LemiMetadata.load_md(filename)
        run_list = list(set(self.lemi_md.run_list) & set(lemi_md_saved.run_list))
        # Set/Reset metadata at network level
        self.lemi_md.net = lemi_md_saved.net
        # Set/Reset metadata at station level
        # Reset station name to None to ensure that the user checks preloaded
        # metadata before conversion (per data group suggestion)
        lemi_md_saved.sta.archive_id = None
        self.lemi_md.sta = lemi_md_saved.sta
        # Set/Reset metadata at run level
        # Instantiate Run data class with proper run_id, start and end times if
        # field sheets were not parsed and initialize GUI attributes (only)
        # using previously saved ones.
        if not self.lemi_md.run:
            self.lemi_md.run.extend(self.lemi_md.init_run_props())
        for x in lemi_md_saved.run:
            if x.run_id not in run_list:
                continue
            ind_run = self.lemi_md.run_list.index(x.run_id)
            md_props = x.__dict__
            md_gui = [md_field.name for md_field in fields(x) if md_field.metadata.get('gui')]
            for key, val in md_props.items():
                if key in md_gui:
                    setattr(self.lemi_md.run[ind_run], key, val)
                if key in self.lemi_md.run[ind_run].md_missing:
                    self.lemi_md.run[ind_run].md_missing.discard(key)
                if key in self.lemi_md.run[ind_run].md_invalid:
                    self.lemi_md.run[ind_run].md_invalid.discard(key)
        # Set/Reset metadata at data channel level
        self.lemi_md.elec = [x for x in lemi_md_saved.elec if x.run_id in run_list]
        self.lemi_md.mag = [x for x in lemi_md_saved.mag if x.run_id in run_list]
        self.set_model()
        for run_id in run_list:
            self.update_elec_selection_forms(run_id)

    def load_md(self, path2savedmd=None):
        """Load previously saved metadata model."""
        if path2savedmd:
            filename = str(path2savedmd)
        else:
            filename, _ = QtWidgets.QFileDialog.getOpenFileName(parent=self,
                                                                caption='Select metadata file to open',
                                                                dir=str(Path.cwd()),
                                                                filter='Metadata file (*.pkl)')
        if filename:
            text = "Loading previously saved metadata..."
            details = ("If you are creating mseed and StationXML files for a "
                       "different station than the one for which you are "
                       "uploading previously saved metadata, please check the "
                       "validity of all your metadata fields before proceeding "
                       "with conversion!")
            icon = QtWidgets.QMessageBox.Information
            msg_box = self.create_message_box(text, details, icon)
            msg_box.exec()
            self.reset_model(filename)

    def save_md(self):
        """Save current metadata model."""
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(parent=self,
                                                            caption='Save inputted metadata',
                                                            dir=str(Path.cwd().joinpath('lemi_metadata.pkl')),
                                                            filter='Metadata file (*.pkl)')
        if filename:
            self.lemi_md.save_md(filename)

    def get_run_widgets(self, run_id):
        return getattr(self, f'run_{run_id}_widgets')

    def set_run_widgets(self, run_id, val):
        setattr(self, f'run_{run_id}_widgets', val)

    def set_num_runs_form(self):
        """
        Set up number of runs form. There is one form for all runs. This form
        is non-editable and reports to the user how many runs were recorded.
        """
        layout_num_runs = QtWidgets.QFormLayout()
        uiNumRuns = QtWidgets.QLineEdit()
        uiNumRuns.setText(str(self.num_runs))
        uiNumRuns.setReadOnly(True)
        uiNumRuns.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        uiNumRuns.setMinimumWidth(30)
        layout_num_runs.addRow('Number of runs', uiNumRuns)
        layout_num_runs.setFormAlignment(QtCore.Qt.AlignLeft)
        return layout_num_runs

    def set_run_starttime_form(self, run_id):
        """
        Set up form for the start time of a given run. There is one form per
        run id. This form is non-editable and provides additional background
        information to the user.
        """
        # Get run start time
        ind_run = self.run_list.index(run_id)
        run_nums = [x['run_num'] for x in self.lemi_data.data_np]
        starttimes = [x['starttime'] for x in self.lemi_data.data_np]
        # Add 1 to ind_run since run_nums (number of given run) starts at 1
        starttime = starttimes[run_nums.index(ind_run + 1)]
        starttime_reformat = starttime.datetime.strftime("%Y-%m-%d %H:%M:%S")
        # Set up layout form
        layout_run_starttime = QtWidgets.QFormLayout()
        uiRunStarttime = QtWidgets.QLineEdit()
        uiRunStarttime.setText(starttime_reformat)
        uiRunStarttime.setReadOnly(True)
        uiRunStarttime.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        uiRunStarttime.setMinimumWidth(175)
        layout_run_starttime.addRow('Start time of run', uiRunStarttime)
        layout_run_starttime.setFormAlignment(QtCore.Qt.AlignLeft)
        return layout_run_starttime

    def submit_run_cha_md(self, run_id, elec_bool=False, mag_bool=False):
        """
        Submit run and channel metadata entries provided by the user up to this
        point before checking validity of metadata fields to be copied.
        Submit policy set in BaseWidget is Manual.
        """
        # -- Run --
        run = self.get_run_widgets(run_id)
        run.mapper.submit()
        # -- Electric --
        if elec_bool:
            name = f'run_{run_id}_num_e_pairs'
            num_e_pairs_submit = getattr(self, name, NUM_E_CHA_MAX)
            for ind in range(1, num_e_pairs_submit + 1):
                e_pair = self.get_elec_pair_widgets(run_id, ind)
                e_pair.mapper.submit()
        # -- Magnetic --
        if mag_bool:
            mag = self.get_mag_widgets(run_id)
            mag.mapper.submit()

    def check_before_reset(self, run_id_same_as, run_id, type_):
        """
        Check/Validate metadata fields for both runs before copying electric or
        magnetic metadata fields from one run to the other.
        """
        cha_type = MSG_MAP[type_.capitalize()]
        logger.info(f"{'-'*20} Checking metadata fields for run '{run_id_same_as}' "
                    f"before copying {cha_type} metadata fields from run "
                    f"'{run_id_same_as}' to run '{run_id}'. Any incomplete or "
                    "invalid metadata fields may prevent the auto-filling "
                    "process from succesfully completing. Look for potential"
                    f"error messages below {'-'*20}.")
        self.validate_run(run_id_same_as)
        if cha_type != 'run':
            validate_method = f"validate_{type_}"
            methodcaller(validate_method, run_id_same_as)(self)
        logger.info(f"{'-'*20} End of metadata field checks for run "
                    f"'{run_id_same_as}' {'-'*20}.")

    def reset_run_model(self, run_id_same_as, run_id):
        """
        Reset current run metadata model based on a user-selected run id.
        Called when selecting the "Same as run" option for run_id > a.
        """
        self.submit_run_cha_md(run_id_same_as)
        self.check_before_reset(run_id_same_as, run_id, 'run')
        md = self.lemi_md.for_gui
        key = f'Run_{run_id}'
        key_same_as = f'Run_{run_id_same_as}'
        md['Run'][key].update(md['Run'][key_same_as])
        model = TreeModel(("Field", "Value"), md)
        run = self.get_run_widgets(run_id)
        run.set_model(model, f'Run_{run_id}')

    def set_cha_same_as_forms(self, cha_type, run_id):
        """
        Set up "same as run" form.
        Called when there is more than one run and selecting the "Same as run option.
        """
        run_ids = self.run_list[:self.run_list.index(run_id)]
        layout_same_as = QtWidgets.QFormLayout()
        uiRunIdSameAs = QtWidgets.QComboBox()
        uiRunIdSameAs.addItems(run_ids)
        for ind, id_ in enumerate(run_ids):
            tip_msg = f'Same {cha_type} metadata field values as Run {id_}.'
            uiRunIdSameAs.setItemData(ind, tip_msg, QtCore.Qt.ToolTipRole)
        reset_method = f'reset_{cha_type}_model'
        uiRunIdSameAs.textActivated.connect(lambda text, val=run_id: methodcaller(reset_method, text, val)(self))
        layout_same_as.addRow('Same as run', uiRunIdSameAs)
        layout_same_as.setFormAlignment(QtCore.Qt.AlignLeft)
        return layout_same_as

    def set_run_editor_forms(self):
        """Set up run editor form. There is one form per run id."""
        layout_num_runs = self.set_num_runs_form()
        self.sta_widgets.uiRunsEditor.addLayout(layout_num_runs)
        self.sta_widgets.uiRunsEditor.addSpacing(5)
        uiTabs = QtWidgets.QTabWidget()
        setattr(self, 'uiRunTabs', uiTabs)
        self.sta_widgets.uiRunsEditor.addWidget(uiTabs)
        for run_id in self.run_list:
            uiRun = QtWidgets.QWidget()
            layout_run_option = QtWidgets.QHBoxLayout()
            layout_run_starttime = self.set_run_starttime_form(run_id)
            layout_run_option.addLayout(layout_run_starttime)
            if run_id != 'a':
                layout_run_same_as = self.set_cha_same_as_forms('run', run_id)
                layout_run_option.addLayout(layout_run_same_as)
            layout_run = QtWidgets.QVBoxLayout()
            layout_run.addLayout(layout_run_option)
            layout_run.addSpacing(5)
            self.set_run_widgets(run_id, RunWidgets(self))
            layout_run.addWidget(self.get_run_widgets(run_id))
            uiRun.setLayout(layout_run)
            setattr(self, f'uiRun_{run_id}', uiRun)
            uiTabs.addTab(uiRun, 'Run ' + run_id)

    def set_elec_pair_widgets(self, run_id, ind, val):
        setattr(self, f'run_{run_id}_elec_pair_{ind}_widgets', val)

    def get_elec_pair_widgets(self, run_id, ind):
        return getattr(self, f'run_{run_id}_elec_pair_{ind}_widgets')

    def update_combo_chas(self, ind, params):
        """
        Update channel port combo box values based on channel port selected
        for other channels.
        Used for both the electric and magnetic channels.
        """
        run_id, ind_e_pair = params
        e_pair_1 = self.get_elec_pair_widgets(run_id, ind_e_pair)
        combo_parent = e_pair_1.uiChaPort
        e_pair_2 = self.get_elec_pair_widgets(run_id, ind_e_pair + 1)
        combo_child = e_pair_2.uiChaPort
        combo_child.clear()
        chas = combo_parent.itemData(ind)
        if chas is not None:
            for i in range(len(chas)):
                combo_child.addItem(chas[i], [x for x in chas if x != chas[i]])

    def set_elec_pair_forms(self, run_id, ElecWidgets):
        """
        Set up the electrode pair form. There is one form per electric channel
        and that form includes all the electric metadata fields required for
        archival.
        """
        layout_elec_pairs = QtWidgets.QHBoxLayout()
        chas = [f'E{i+1}' for i in range(NUM_E_CHA_MAX)]
        for ind in range(1, NUM_E_CHA_MAX + 1):
            self.set_elec_pair_widgets(run_id, ind, ElecWidgets(self, 'elec'))
            e_pair = self.get_elec_pair_widgets(run_id, ind)
            # Set up channel port combo box for electrode pair 1.
            if ind == 1:
                for i in range(NUM_E_CHA_MAX):
                    e_pair.uiChaPort.addItem(chas[i], [x for x in chas if x != chas[i]])
            # Set up channel port combo box for electrode pairs 2, 3 and 4 based
            # on parent channel port combo boxes (respectively 1, 2 and 3).
            if ind > 1:
                parent = self.get_elec_pair_widgets(run_id, ind-1).uiChaPort
                self.update_combo_chas(parent.currentIndex(), [run_id, ind-1])
            # Value of the channel port for electrode pair 4 cannot be directly
            # selected by the user. Undirectly updated by updating parent combo
            # boxes.
            if ind < NUM_E_CHA_MAX:
                e_pair.uiChaPort.currentIndexChanged.connect(lambda ind_, params=[run_id, ind]: self.update_combo_chas(ind_, params))  # noqa: E501
            e_pair.groupBox.setTitle(' '.join(['Electrode Pair', str(ind)]))
            layout_elec_pairs.addWidget(e_pair)
        return layout_elec_pairs

    def update_elec_pair_forms(self, num_e_pairs, run_id):
        """
        Disable or enable electrode pair form based on the selected number of
        electrode pairs.
        """
        for ind in range(1, NUM_E_CHA_MAX + 1):
            e_pair = self.get_elec_pair_widgets(run_id, ind)
            if ind in range(1, int(num_e_pairs) + 1):
                e_pair.groupBox.setEnabled(True)
            else:
                e_pair.groupBox.setEnabled(False)
        setattr(self, f'run_{run_id}_num_e_pairs', int(num_e_pairs))

    def set_elec_selection_forms(self, run_id):
        """
        Set up electrode selection form. There is one form per run id. That
        form allows the user to select how many electrode pairs were deployed
        for that specific run.
        Based on the selected number, the electrode pair form for the
        non-deployed pair(s) will be disabled.
        """
        layout_elec_selection = QtWidgets.QFormLayout()
        uiNumEPairs = QtWidgets.QComboBox()
        uiNumEPairs.addItems([str(x) for x in VALID_NUM_E_PAIRS])
        for ind, num in enumerate(VALID_NUM_E_PAIRS):
            tip_msg = f'Number of electrode pairs deployed: {num} positive and {num} negative electrode(s)'
            uiNumEPairs.setItemData(ind, tip_msg, QtCore.Qt.ToolTipRole)
        uiNumEPairs.setObjectName(f'run_{run_id}_num_e_pairs')
        uiNumEPairs.textActivated.connect(lambda text, val=run_id: self.update_elec_pair_forms(text, val))
        setattr(self, f'run_{run_id}_num_e_pairs_widgets', uiNumEPairs)
        layout_elec_selection.addRow('Number of electrode pairs *', uiNumEPairs)
        layout_elec_selection.setFormAlignment(QtCore.Qt.AlignLeft)
        return layout_elec_selection

    def update_elec_selection_forms(self, run_id):
        """
        Update electrode selection form for given run_id if the number of
        electrode pairs can be infered from the 'components recorded' metadata
        field.
        """
        e_comps = self.lemi_md.get_comps_rec('E', run_id) if self.lemi_md.run else []
        num_e_pairs = len(e_comps)
        uiNumEPairs = getattr(self, f'run_{run_id}_num_e_pairs_widgets')
        uiNumEPairs.setCurrentIndex(VALID_NUM_E_PAIRS.index(num_e_pairs))
        self.update_elec_pair_forms(num_e_pairs, run_id)

    def reset_elec_model(self, run_id_same_as, run_id):
        """
        Reset current electric metadata model based on a user-selected run id.
        Called when selecting the "Same as run" option for run_id > a.
        """
        self.submit_run_cha_md(run_id_same_as, elec_bool=True)
        self.check_before_reset(run_id_same_as, run_id, 'elec')
        md = self.lemi_md.for_gui
        for ind_cha in range(1, NUM_E_CHA_MAX + 1):
            key = f'Run_{run_id}_Elec_Pair_{ind_cha}'
            key_same_as = f'Run_{run_id_same_as}_Elec_Pair_{ind_cha}'
            md['Elec'][key].update(md['Elec'][key_same_as])
        model = TreeModel(("Field", "Value"), md)
        for ind_cha in range(1, NUM_E_CHA_MAX + 1):
            e_pair = self.get_elec_pair_widgets(run_id, ind_cha)
            e_pair.set_model(model, f'Run_{run_id}_Elec_Pair_{ind_cha}')

    def set_elec_editor_forms(self, ElecWidgets):
        """
        Set up the electric editor form for all runs.
        This form includes the electrode selection form, electrode pair form
        and "same as run" form (if the run id > a).
        """
        uiTabs = QtWidgets.QTabWidget()
        setattr(self, 'uiRunTabs_Elec', uiTabs)
        self.uiElecEditor.addWidget(uiTabs)
        for run_id in self.run_list:
            layout_elec_pairs = self.set_elec_pair_forms(run_id, ElecWidgets)
            layout_elec_option = QtWidgets.QHBoxLayout()
            layout_elec_selection = self.set_elec_selection_forms(run_id)
            self.update_elec_selection_forms(run_id)
            layout_elec_option.addLayout(layout_elec_selection)
            if run_id != 'a':
                layout_elec_same_as = self.set_cha_same_as_forms('elec', run_id)
                layout_elec_option.addLayout(layout_elec_same_as)
            layout_elec = QtWidgets.QVBoxLayout()
            layout_elec.addLayout(layout_elec_option)
            layout_elec.addLayout(layout_elec_pairs)
            uiRun = QtWidgets.QWidget()
            uiRun.setLayout(layout_elec)
            setattr(self, f'uiRun_{run_id}_Elec', uiRun)
            uiTabs.addTab(uiRun, 'Run ' + run_id)

    def set_mag_widgets(self, run_id, val):
        setattr(self, f'run_{run_id}_mag_widgets', val)

    def get_mag_widgets(self, run_id):
        return getattr(self, f'run_{run_id}_mag_widgets')

    def reset_mag_model(self, run_id_same_as, run_id):
        """
        Reset current magnetic metadata model based on a user-selected run id.
        Called when selecting the "Same as run" option for run_id > a.
        """
        self.submit_run_cha_md(run_id_same_as, mag_bool=True)
        self.check_before_reset(run_id_same_as, run_id, 'mag')
        md = self.lemi_md.for_gui
        key = f'Run_{run_id}_Mag'
        key_same_as = f'Run_{run_id_same_as}_Mag'
        md['Mag'][key].update(md['Mag'][key_same_as])
        model = TreeModel(("Field", "Value"), md)
        mag = self.get_mag_widgets(run_id)
        mag.set_model(model, f'Run_{run_id}_Mag')

    def set_mag_editor_forms(self, MagWidgets):
        """
        Set up the magnetic editor form for all runs.
        This form includes the magnetic pair form and "same as run" form (if
        the run id > a).
        """
        uiTabs = QtWidgets.QTabWidget()
        setattr(self, 'uiRunTabs_Mag', uiTabs)
        self.uiMagEditor.addWidget(uiTabs)
        for run_id in self.run_list:
            self.set_mag_widgets(run_id, MagWidgets(self, 'mag'))
            layout_mag = QtWidgets.QVBoxLayout()
            if run_id != 'a':
                layout_mag_same_as = self.set_cha_same_as_forms('mag', run_id)
                layout_mag.addLayout(layout_mag_same_as)
            layout_mag.addWidget(self.get_mag_widgets(run_id))
            layout_mag.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
            uiRun = QtWidgets.QWidget()
            uiRun.setLayout(layout_mag)
            setattr(self, f'uiRun_{run_id}_Mag', uiRun)
            uiTabs.addTab(uiRun, 'Run ' + run_id)

    def set_model(self):
        """
        Set tree model. Includes metadata at the net, sta, run, elec and mag
        levels.
        """
        model = TreeModel(("Field", "Value"), self.lemi_md.for_gui)
        self.net_widgets.set_model(model)
        self.sta_widgets.set_model(model)
        for run_id in self.run_list:
            run = self.get_run_widgets(run_id)
            run.set_model(model, f'Run_{run_id}')
            mag = self.get_mag_widgets(run_id)
            mag.set_model(model, f'Run_{run_id}_Mag')
        for run_id, ind_cha in get_e_ids(self.num_runs):
            e_pair = self.get_elec_pair_widgets(run_id, ind_cha)
            e_pair.set_model(model, f'Run_{run_id}_Elec_Pair_{ind_cha}')

    def flag_tab_bar(self, tabwidget, cat, num_missing_invalid):
        """
        Flag panel name in the main window if metadata fields under that panel
        are either invalid or missing.
        """
        bar = tabwidget.tabBar()
        ind = tabwidget.indexOf(getattr(self, f'ui{cat}'))
        if num_missing_invalid > 0:
            bar.setTabTextColor(ind, QtGui.QColor(255, 0, 0))
        else:
            bar.setTabTextColor(ind, QtGui.QColor(0, 0, 0))

    def flag_net_md(self):
        """"
        Flag network metadata fields that are required but unfilled or invalid."""
        req_fields_n = self.lemi_md.net.md_missing | self.lemi_md.net.md_invalid
        self.net_widgets.set_flags(req_fields_n)
        num_missing_invalid_n = len(req_fields_n)
        self.flag_tab_bar(self.uiMainTabs, 'Net', num_missing_invalid_n)
        return num_missing_invalid_n

    def validate_net(self):
        """Validate network metadata entry."""
        md_fields_n = self.net_widgets.model
        self.lemi_md.populate_net_props(md_fields_n)
        num_missing_invalid_n = self.flag_net_md()
        return num_missing_invalid_n

    def flag_sta_md(self):
        """Flag station metadata fields that are required but unfilled or invalid."""
        req_fields_s = self.lemi_md.sta.md_missing | self.lemi_md.sta.md_invalid
        self.sta_widgets.set_flags(req_fields_s)
        num_missing_invalid_s = len(req_fields_s)
        self.flag_tab_bar(self.uiMainTabs, 'Sta', num_missing_invalid_s)
        return num_missing_invalid_s

    def validate_sta(self):
        """Validate station metadata entry."""
        md_fields_s = self.sta_widgets.model
        self.lemi_md.populate_sta_props(md_fields_s)
        num_missing_invalid_s = self.flag_sta_md()
        return num_missing_invalid_s

    def flag_run_md(self, run_list):
        """Flag run metadata fields that are required but unfilled or invalid."""
        req_fields_r = [run.md_missing | run.md_invalid
                        for run in self.lemi_md.run
                        if run.resource_id.split(':')[1] in run_list]
        num_missing_invalid_r = sum([len(x) for x in req_fields_r])
        for ind, run_id in enumerate(run_list):
            run = self.get_run_widgets(run_id)
            run.set_flags(req_fields_r[ind])
            self.flag_tab_bar(self.uiRunTabs, f'Run_{run_id}', len(req_fields_r[ind]))
        num_missing_invalid_s = getattr(self, 'num_missing_invalid_s', 0)
        if num_missing_invalid_s == 0:
            self.flag_tab_bar(self.uiMainTabs, 'Sta', num_missing_invalid_r)
        return num_missing_invalid_r

    def validate_run(self, run_id=None):
        """Validate run metadata entry."""
        run_list = self.run_list if run_id is None else [run_id]
        md_fields_r = {f'Run_{run_id}': attrgetter(f'run_{run_id}_widgets.model')(self)
                       for run_id in run_list}
        self.lemi_md.populate_run_props(md_fields_r, run_list)
        num_missing_invalid_r = self.flag_run_md(run_list)
        return num_missing_invalid_r

    def flag_elec_md(self, run_list, num_e_pairs):
        """Flag electric metadata fields that are required but unfilled or invalid."""
        num_missing_invalid_e = 0
        for run_id in run_list:
            num_e_pairs_run = num_e_pairs.get(run_id)
            if num_e_pairs_run is None:
                continue
            req_fields = {x.pair_num: x.md_missing | x.md_invalid
                          for x in self.lemi_md.filter_cha('elec', run_id)}
            # Flag num_e_pairs at the elec level if the number of electrode
            # pairs does not match the number of electric channels in
            # components_recorded
            widget_style = ""
            e_field = f'run_{run_id}_num_e_pairs'
            if e_field in [y for k, v in req_fields.items() for y in v]:
                widget_id = f'QComboBox#{e_field}'
                widget_style = f'{widget_id} {{padding: 10px; border:2px ridge red;}}'
            uiRun = getattr(self, f'uiRun_{run_id}_Elec')
            uiRun.setStyleSheet(widget_style)
            # Flag every other required electric metadata fields that don't
            # pass validation
            num = 0
            for pair_num, req_field in req_fields.items():
                key = f'Run_{run_id}_Elec_Pair_{pair_num}'
                e_pair = getattr(self, f'{key.lower()}_widgets')
                # The electrode pair number for a run ranges from one to the
                # number of electrode pairs deployed during that given run.
                if pair_num in range(1, num_e_pairs_run+1):
                    e_pair.set_flags(req_field)
                    num += len(req_field)
                else:
                    e_pair.set_flags([])
            num_missing_invalid_e += num
            self.flag_tab_bar(self.uiRunTabs_Elec, f'Run_{run_id}_Elec', num)
        self.flag_tab_bar(self.uiMainTabs, 'Elec', num_missing_invalid_e)
        return num_missing_invalid_e

    def validate_elec(self, run_id=None):
        """Validate electric metadata entry."""
        run_list = self.run_list if run_id is None else [run_id]
        num_e_pairs = {x: getattr(self, f'run_{x}_num_e_pairs', None) for x in run_list}
        # The variable ind_cha defines the electrode pair numbers for a given run.
        # The maximum number of electrode pairs being defined by NUM_E_CHA_MAX.
        md_fields_e = {f'Run_{run_id}_Elec_Pair_{ind_cha}':
                       attrgetter(f'run_{run_id}_elec_pair_{ind_cha}_widgets.model')(self)
                       for ind_cha in range(1, NUM_E_CHA_MAX + 1)
                       for run_id in run_list}
        num_e_pairs = self.lemi_md.populate_elec_props(md_fields_e, run_list, num_e_pairs)
        num_missing_invalid_e = self.flag_elec_md(run_list, num_e_pairs)
        return num_missing_invalid_e

    def flag_mag_md(self, run_list):
        """Flag magnetic metadata fields that are required but unfilled or invalid."""
        num_missing_invalid_m = 0
        for run_id in run_list:
            req_fields_m = [mag.md_missing | mag.md_invalid
                            for mag in self.lemi_md.filter_cha('mag', run_id)]
            if req_fields_m:
                mag = self.get_mag_widgets(run_id)
                mag.set_flags(req_fields_m[0])
                num = len(req_fields_m[0])
                num_missing_invalid_m += num
                self.flag_tab_bar(self.uiRunTabs_Mag, f'Run_{run_id}_Mag', num)
        self.flag_tab_bar(self.uiMainTabs, 'Mag', num_missing_invalid_m)
        return num_missing_invalid_m

    def validate_mag(self, run_id=None):
        """Validate magnetic metadata entry."""
        run_list = self.run_list if run_id is None else [run_id]
        md_fields_m = {f'Run_{run_id}_Mag': attrgetter(f'run_{run_id}_mag_widgets.model')(self)
                       for run_id in run_list}
        self.lemi_md.populate_mag_props(md_fields_m, run_list)
        num_missing_invalid_m = self.flag_mag_md(run_list)
        return num_missing_invalid_m

    def set_validation(self):
        """
        Set up validation of user-provided metadata at all levels (net, sta,
        run, elec, mag) and keep track of number of invalid or missing metadata
        field.
        """
        logger.info(f"{'-'*20} Start - Validation of metadata entry set {'-'*20}")
        num_missing_invalid_n = self.validate_net()
        num_missing_invalid_s = self.validate_sta()
        num_missing_invalid_r = self.validate_run()
        num_missing_invalid_e = self.validate_elec()
        num_missing_invalid_m = self.validate_mag()
        logger.info(f"{'-'*20} End - Validation of metadata entry set {'-'*20}")
        self.num_missing_invalid = sum([num_missing_invalid_n, num_missing_invalid_s,
                                        num_missing_invalid_r, num_missing_invalid_e,
                                        num_missing_invalid_m])

    def display_conversion_msg(self):
        """
        Inform users that they can now convert their data and metadata to an
        archivable format by displaying a message in a pop-up window.
        """
        text = "Data and Metadata Conversion Status..."
        details = ("All required metadata are properly filled out.\n"
                   "You can now convert your data and metadata to an "
                   "archivable format!\nConsider saving the inputted "
                   "metadata (⌘S) before proceeding.")
        icon = QtWidgets.QMessageBox.Information
        msg_box = self.create_message_box(text, details, icon)
        ret = msg_box.exec()
        if ret == QtWidgets.QMessageBox.Ok:
            logger.info('All required metadata have been properly filled out '
                        'by the user. Data and metadata conversion actions '
                        'have been enabled.')

    def enable_conversion(self):
        """
        Enable data and metadata conversion icons if all required metadata are
        valid.
        """
        num_missing_invalid = getattr(self, 'num_missing_invalid', None)
        if num_missing_invalid == 0:
            self.actionWrite_mseed.setEnabled(True)
            self.actionWrite_StationXML.setEnabled(True)
            self.display_conversion_msg()
        else:
            self.actionWrite_mseed.setEnabled(False)
            self.actionWrite_StationXML.setEnabled(False)

    def display_mseed_msg(self):
        """
        Inform users that the day-long mseed files were successfully written.
        """
        text = "Data Conversion Complete!"
        details = ("Writing of the daylong mseed files for each channel and "
                   "location code was successfully completed.")
        icon = QtWidgets.QMessageBox.Information
        msg_box = self.create_message_box(text, details, icon)
        msg_box.exec()

    def write_mseed_steps(self):
        """Steps to write day-long mseed files."""
        self.lemi_data.update_data(md=self.lemi_md)
        write_daylong_mseed(self.lemi_data)
        if self.lemi_data.def_files:
            write_log_file(self.lemi_data)
        self.display_mseed_msg()

    def display_stationxml_msg(self):
        """
        Inform users that the StationXML files were successfully written.
        """
        text = "Metadata Conversion Complete!"
        details = "Writing of the StationXML file was successfully completed."
        icon = QtWidgets.QMessageBox.Information
        msg_box = self.create_message_box(text, details, icon)
        msg_box.exec()

    def write_stationxml_steps(self):
        """Steps to write StationXML files."""
        self.lemi_md.populate_aux_props()
        self.lemi_md.update_azimuth_tilt()
        self.lemi_data.update_data(md=self.lemi_md)
        write_stationxml(self.lemi_md)
        self.display_stationxml_msg()

    def processing_conversion_steps(self):
        """
        Set up processing and conversion steps. The processing steps consists
        in submitting the metadata entries and validating the user-provided
        metadata at all levels (net, sta, run, elec, magnetic).
        The conversion steps include writing day-long mseed and StationXML files.
        """
        # --- Submit metadata entries ---
        self.uiValidateMD.clicked.connect(self.net_widgets.mapper.submit)
        self.uiValidateMD.clicked.connect(self.sta_widgets.mapper.submit)
        for run_id in self.run_list:
            # -- Run --
            run = self.get_run_widgets(run_id)
            self.uiValidateMD.clicked.connect(run.mapper.submit)
            # -- Electric --
            for ind in range(1, NUM_E_CHA_MAX + 1):
                e_pair = self.get_elec_pair_widgets(run_id, ind)
                self.uiValidateMD.clicked.connect(e_pair.mapper.submit)
            # -- Magnetic --
            mag = self.get_mag_widgets(run_id)
            self.uiValidateMD.clicked.connect(mag.mapper.submit)
        # --- Validate metadata entries ---
        self.uiValidateMD.clicked.connect(lambda checked: self.set_validation())
        # --- Convert data and metadata to archivable format ---
        self.uiValidateMD.clicked.connect(lambda checked: self.enable_conversion())
        self.actionWrite_mseed.triggered.connect(lambda checked: self.write_mseed_steps())
        self.actionWrite_StationXML.triggered.connect(lambda checked: self.write_stationxml_steps())

    def close_all_windows(self):
        # Close "Help" window
        self.help_window.close()
        # Close "Electrode - Specs" and "Fluxgate - Specs" windows
        for run_id in self.run_list:
            mag = self.get_mag_widgets(run_id)
            mag.inst_widgets.close()
            for ind in range(1, NUM_E_CHA_MAX + 1):
                e_pair = self.get_elec_pair_widgets(run_id, ind)
                e_pair.inst_widgets.close()

    def closeEvent(self, event):
        """
        Reimplement closeEvent() event handler to detect when the user attempts
        to close the main window and ask whether they want to save any changes
        made.
        """
        text = "Exiting lemi2seed application..."
        details = "Do you want to save any pending changes?"
        icon = QtWidgets.QMessageBox.Question
        msg_box = self.create_message_box(text, details, icon)
        msg_box.setStandardButtons(QtWidgets.QMessageBox.Save |
                                   QtWidgets.QMessageBox.Discard |
                                   QtWidgets.QMessageBox.Cancel)
        msg_box.setDefaultButton(QtWidgets.QMessageBox.Save)
        msg_box.setEscapeButton(QtWidgets.QMessageBox.Cancel)
        msg_box.buttonClicked.connect(self.close_all_windows)
        ret = msg_box.exec()
        if ret == QtWidgets.QMessageBox.Save:
            self.save_md()
            event.accept()
        elif ret == QtWidgets.QMessageBox.Discard:
            event.accept()
        else:
            event.ignore()
