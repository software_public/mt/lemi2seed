# -*- coding: utf-8 -*-
"""
lemi2seed GUI Framework - Widgets.

Maeva Pourpoint - IRIS/PASSCAL
"""

import sys

from pathlib import Path
from PySide6 import QtCore, QtWidgets
from PySide6.QtUiTools import loadUiType

# Handle issues importing resources (allows python to find module resources_rc)
sys.path.append(str(Path(__file__).resolve().parent))


def load_ui(filename):
    """Load a ui file relative to its source file."""
    path = Path(__file__).resolve().parent.joinpath('ui_files', filename)
    return loadUiType(str(path))


class BaseWidget:

    """
    Implements the basic functionalities of all category widgets: Net, Sta, Run,
    Elec, Mag.
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.set_mapper()

    def set_flags(self, fields):
        """
        Flag each missing or invalid metadata field by applying a solid red
        border to its input widget.
        """
        widget_ids = []
        for field in fields:
            field = "".join(['ui', *[s.capitalize() for s in field.split('_')]])
            if hasattr(self, field):
                widget_ids.append("#".join([type(getattr(self, field)).__name__, field]))
        if widget_ids:
            widget_ids = ", ".join(widget_ids)
            widget_styles = f'{widget_ids} {{border:2px ridge red;}}'
        else:
            widget_styles = ""
        self.setStyleSheet(widget_styles)

    def set_mapper(self):
        self.mapper = QtWidgets.QDataWidgetMapper()
        self.mapper.setSubmitPolicy(QtWidgets.QDataWidgetMapper.ManualSubmit)
        self.mapper.setOrientation(QtCore.Qt.Vertical)

    def set_mapping(self, parent):
        self.mapper.setRootIndex(parent)
        for ind, child_item in enumerate(self.mapper.model().get_item(parent).child_items):
            field = child_item.data(0)
            field = "".join(['ui', *[s.capitalize() for s in field.split('_')]])
            if not hasattr(self, field):
                continue
            self.mapper.addMapping(getattr(self, field), ind)

    def set_model(self, model, cat, set_selection=True):
        if model.input_data.get(cat):
            self.model = model.input_data[cat]
        else:
            if 'Elec' in cat:
                self.model = model.input_data['Elec'][cat]
            elif 'Mag' in cat:
                self.model = model.input_data['Mag'][cat]
            else:
                self.model = model.input_data['Run'][cat]
        self.mapper.setModel(model)
        self.set_mapping(parent=model.category_index[cat])
        if set_selection:
            self.mapper.setCurrentIndex(1)


class NetWidgets(BaseWidget, *load_ui("networkwidget.ui")):

    def __init__(self, parent=None):
        super().__init__(parent)

    def set_model(self, model):
        super().set_model(model, cat='Net')


class StaWidgets(BaseWidget, *load_ui("stationwidget.ui")):

    def __init__(self, parent=None):
        super().__init__(parent)
        for combobox in [self.uiDeclinationModel, self.uiOrientationMethod]:
            combobox.textActivated.connect(lambda text, val=combobox: self.other_option(text, val))

    def set_model(self, model):
        super().set_model(model, cat='Sta')

    @staticmethod
    def other_option(val, combobox):
        if val == 'If other, enter here.':
            if combobox.findText(val) != combobox.count() - 1:
                combobox.removeItem(combobox.count() - 1)
            combobox.setEditable(True)
            combobox.currentTextChanged.connect(combobox.setEditText)
        else:
            combobox.setEditable(False)


class RunWidgets(BaseWidget, *load_ui("runwidget.ui")):

    def __init__(self, parent=None):
        super().__init__(parent)

    def set_model(self, model, cat):
        super().set_model(model, cat=cat)


def get_channel_widgets(base):

    class ChannelWidgets(*base):

        def __init__(self, parent=None, type_=None):
            super().__init__(parent)
            self.inst_widgets = InstInfoWidgets(type_)
            self.uiInstSpecs.textActivated.connect(self.toggle_inst_window)
            self.inst_widgets.uiSaveButton.clicked.connect(self.show_inst_info)

        def show_inst_info(self):
            ind = self.uiInstSpecs.count() - 1
            text = ("Manufacturer: {} - Model: {} - Type: {}"
                    .format(self.inst_widgets.uiManufacturer.text(),
                            self.inst_widgets.uiModel.text(),
                            self.inst_widgets.uiType.text()))
            self.uiInstSpecs.insertItem(ind, text)
            self.uiInstSpecs.setCurrentIndex(ind)
            self.inst_widgets.hide()

        def set_model(self, model, cat):
            super().set_model(model, cat=cat, set_selection=False)
            parent = model.get_item(model.category_index[cat])
            self.mapper.addMapping(self.inst_widgets.uiManufacturer,
                                   parent.get_child_index(0, 'inst_manufacturer'))
            self.mapper.addMapping(self.inst_widgets.uiModel,
                                   parent.get_child_index(0, 'inst_model'))
            self.mapper.addMapping(self.inst_widgets.uiType,
                                   parent.get_child_index(0, 'inst_type'))
            self.mapper.setCurrentIndex(1)

        def toggle_inst_window(self, val):
            if self.inst_widgets.isVisible():
                self.inst_widgets.hide()
            elif val == "If other, click here.":
                self.inst_widgets.show()

    return ChannelWidgets


class InstInfoWidgets(*load_ui("instrumentwidget.ui")):

    def __init__(self, type_):
        super().__init__()
        self.setupUi(self)
        if type_ == 'elec':
            self.setWindowTitle('Electrode - Specs')
        else:
            self.setWindowTitle('Fluxgate - Specs')


class HelpWidgets(*load_ui("helpwidget.ui")):

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Help')
