# -*- coding: utf-8 -*-
"""
Routines for data prep before conversion to mseed files.

Maeva Pourpoint - IRIS/PASSCAL
"""

from __future__ import annotations

import numpy as np
import re
import sys
import typing

from obspy import UTCDateTime
from pathlib import Path
from statistics import mean
from typing import Dict, List, Optional, TYPE_CHECKING

from lemi2seed.logging import setup_logger
from lemi2seed.utils import (convert_time, convert_coord, eval_loc_code,
                             get_e_loc, get_run_list, get_run_num)

if TYPE_CHECKING:
    from lemi2seed.lemi_metadata import LemiMetadata

# Set up logging
logger = setup_logger(__name__)

CHA_NAMING_CONV = {'Ex': 'LQN', 'Ey': 'LQE', 'Hx': 'LFN', 'Hy': 'LFE',
                   'Hz': 'LFZ', 'Ui': 'LEH', 'Te': 'LKH', 'Tf': 'LKF',
                   'Sn': 'GNS', 'Fq': 'GST', 'Ce': 'LCE'}
DATA_NUM_COLUMNS = 24
DATA_IND = {'Time': range(0, 6), 'Hx': 6, 'Hy': 7, 'Hz': 8, 'Te': 9, 'Tf': 10,
            'E1': 11, 'E2': 12, 'E3': 13, 'E4': 14, 'Ui': 15, 'Elev': 16,
            'Lat': 17, 'Lat_Hem': 18, 'Lon': 19, 'Lon_Hem': 20, 'Sn': 21,
            'Fq': 22, 'Ce': 23}
DATA_TO_ARCHIVE = ['E1', 'E2', 'E3', 'E4', 'Hx', 'Hy', 'Hz', 'Ui', 'Te', 'Tf',
                   'Sn', 'Fq', 'Ce']
SAMPLING_RATE = 1.0  # Hz
VALID_E_COMPS = ['E1', 'E2', 'E3', 'E4']


class LemiData():
    """
    This class handles the parsing and prep of the data outputted by LEMI-424.
    """

    def __init__(self, path2data: Path, output_mseed: Path, output_log: Path) -> None:
        """Instantiate LemiData class and scan data directory."""
        self.path2data = path2data
        self.output_mseed = output_mseed
        self.output_log = output_log
        self.data_files: List[Path] = []
        self.inf_files: List[Path] = []
        self.def_files: List[Path] = []
        num_runs = self.scan_path2data()
        self.set_stats(num_runs)

    def scan_path2data(self) -> int:
        """
        Scan data directory provided by the user for LEMI generated data files.
        Data directory: .../NET/STA/ where NET is the network code and STA is
        the station name.
        LEMI-424 filenaming convention: YYYYMMDDHHMM.[TXT,INF]
        - Example of data file path: .../NET/STA/DATA0110/202009302105.TXT
        - Example of inf file path: .../NET/STA/DATA0110/202009302104.INF
        - Example of def file path: .../NET/STA/DATA0110/lemi424N0xxx.def
          where xxx is the data logger SN.
        Return number of runs based on number of data acquisition starts
        (or inf files). The minimum number of runs is 1.
        """
        data_files = [tmp for tmp in self.path2data.glob('*/*')
                      if re.match(r"^\w{12}.txt$", tmp.parts[-1], re.IGNORECASE)]
        inf_files = [tmp for tmp in self.path2data.glob('*/*')
                     if re.match(r"^\w{12}.inf$", tmp.parts[-1], re.IGNORECASE)]
        def_files = [tmp for tmp in self.path2data.glob('*/*')
                     if re.match(r"^lemi424N\d{4}.def$", tmp.parts[-1], re.IGNORECASE)]
        if not data_files:
            logger.error("No data files found under the following path - {}. "
                         "Check data path!".format(self.path2data))
            sys.exit(1)
        if not def_files:
            logger.warning("No def files found under the following path - {}. "
                           "Calibration coefficients used by the LEMI424 logger "
                           "to 'internally correct' the recorded data will not "
                           "be archived!".format(self.path2data))
        self.data_files = LemiData.order_files(data_files)
        self.inf_files = LemiData.order_files(inf_files)
        self.def_files = LemiData.order_files(def_files)
        return max(len(inf_files), 1)

    def set_stats(self, num_runs: int) -> None:
        """Initialize set attribute based on number of runs."""
        run_ids = get_run_list(num_runs)
        self.stats: Dict = {'sample_rate': SAMPLING_RATE,
                            'num_runs': num_runs,
                            'dipole_len': {x: {} for x in run_ids},
                            'fluxgate_sn': {x: None for x in run_ids},
                            'datalogger_sn': {x: None for x in run_ids},
                            'acq_start_time': []}

    def parse_inf_files(self) -> None:
        """
        Parse inf files for dipole lengths, logger serial number and acquisition
        start time.
        """
        run_ids = get_run_list(self.stats['num_runs'])
        for ind, inf_file in enumerate(self.inf_files):
            dipole_lens = {}
            with open(inf_file, 'r') as fin:
                for line in fin:
                    line_ = line.strip()
                    if re.search(r"%LEMI424\s#\d{4}", line_):
                        sn = line_.split(' ')[-1][2:]
                    if re.search(r"%L\d\s=\s\d.\d+e\+\d{2}", line_):
                        dipole_len = float(line_.split(' ')[-1])
                        # Otherwise, this indicates that the dipole length wasn't
                        # configured in the field and has a default value of 1.0 m
                        if dipole_len != 1.0:
                            dipole_lens[line_.split(' ')[0].replace('%L', 'E')] = dipole_len
                    if re.search(r"%Date\s\d{4}\/\d{2}\/\d{2}", line_):
                        date = line_.split(' ')[-1]
                    if re.search(r"%Time\s\d{2}:\d{2}:\d{2}", line_):
                        time = line_.split(' ')[-1]
            self.stats['dipole_len'][run_ids[ind]] = dipole_lens
            self.stats['fluxgate_sn'][run_ids[ind]] = sn
            self.stats['datalogger_sn'][run_ids[ind]] = sn
            if date and time:
                self.stats['acq_start_time'] = [*self.stats['acq_start_time'], UTCDateTime(f"{date}T{time}")]

    def parse_data_file(self, data_file: Path) -> Optional[Dict]:
        """Parse data file and check for potential corruptions."""
        data: Dict = {}
        msg = ("The data file - {} - may have been corrupted. Skipping file!"
               .format(data_file))
        with open(data_file, 'r', newline='') as fin:
            for line in fin:
                columns = line.strip().split()
                if line.endswith('\r\n') and len(columns) == DATA_NUM_COLUMNS:
                    tmp = LemiData.reformat_data(columns)
                    if tmp is None:
                        logger.warning(msg)
                        return None
                    for key, val in tmp.items():
                        if data.get(key) is None:
                            data[key] = [val]
                        else:
                            data[key].append(val)
                else:
                    logger.warning(msg)
                    return None
        return data

    def parse_data_files(self) -> None:
        """
        Parse all data files and define data attribute => dictionary structure
        with:
        - key: Time_stamp, Elev, Hx, Hy, Hz, E1, E2, E3, E4, Ui, Te, Tf, Sn,
               Fq, Ce
        - value: list of data points for given key
        """
        self.data: Dict = {}
        for data_file in self.data_files:
            logger.info("Parsing data file - {}.".format(data_file))
            data = self.parse_data_file(data_file)
            if data is not None:
                for key, val in data.items():
                    if self.data.get(key) is None:
                        self.data[key] = val
                    else:
                        self.data[key] = [*self.data[key], *val]

    @staticmethod
    def order_files(files: List[Path]) -> List[Path]:
        """
        Order data files based on their name. Each file name is defined by
        the time stamp of the first data point recorded in the file.
        """
        return sorted(files, key=lambda x: x.parts[-1])

    @typing.no_type_check
    @staticmethod
    def reformat_data(columns: List[str]) -> Optional[Dict]:
        """
        Reformat data in all columns by either applying a time, coordinate or
        float conversion.
        """
        time_stamp = convert_time(' '.join([columns[ind] for ind in DATA_IND['Time']]))
        lat = convert_coord(columns[DATA_IND['Lat']], columns[DATA_IND['Lat_Hem']])
        lon = convert_coord(columns[DATA_IND['Lon']], columns[DATA_IND['Lon_Hem']])
        if not all([time_stamp, lat, lon]):
            return None
        dict_ = {'Time_stamp': time_stamp, 'Lat': lat, 'Lon': lon,
                 'Elev': float(columns[DATA_IND['Elev']]),
                 'Hx': float(columns[DATA_IND['Hx']]),
                 'Hy': float(columns[DATA_IND['Hy']]),
                 'Hz': float(columns[DATA_IND['Hz']]),
                 'E1': float(columns[DATA_IND['E1']]),
                 'E2': float(columns[DATA_IND['E2']]),
                 'E3': float(columns[DATA_IND['E3']]),
                 'E4': float(columns[DATA_IND['E4']]),
                 'Ui': float(columns[DATA_IND['Ui']]),
                 'Te': float(columns[DATA_IND['Te']]),
                 'Tf': float(columns[DATA_IND['Tf']]),
                 'Sn': float(columns[DATA_IND['Sn']]),
                 'Fq': float(columns[DATA_IND['Fq']]),
                 'Ce': float(columns[DATA_IND['Ce']])}
        return dict_

    def detect_gaps(self, time_stamp: List[UTCDateTime]) -> List:
        """
        Detect gaps in time series. The number of gaps correlated with the
        number of acquisition starts defined the number of runs.
        Return list of data point indexes for which a gap is detected.
        """
        diffs = [j - i for i, j in zip(time_stamp[:-1], time_stamp[1:])]
        ind_gap = [ind + 1 for ind, diff in enumerate(diffs)
                   if diff != SAMPLING_RATE]
        if ind_gap:
            logger.warning("Data gaps detected at {}."
                           .format(", ".join([str(time_stamp[x]) for x in ind_gap])))
        ind_gap_run = [i for time in self.stats['acq_start_time']
                       for i in ind_gap if time >= time_stamp[i - 1] and time <= time_stamp[i]]
        if ind_gap_run:
            run_ids = get_run_list(self.stats['num_runs'])
            starttimes = [time_stamp[ind] for ind in [0, *ind_gap_run]]
            logger.info("A total of {} runs were detected. Their start times are: {}"
                        .format(len(run_ids),
                                ', '.join([f'run {x} -> {y}' for x, y in zip(run_ids, starttimes)])))
        return ind_gap_run

    @staticmethod
    def detect_new_day(time_stamp: List[UTCDateTime]) -> List:
        """
        Detect start of a new day in time series. Used to create day-long data
        traces.
        Return list of data point indexes for which a new day is detected.
        """
        return [i + 1 for i in range(len(time_stamp) - 1) if time_stamp[i + 1].day != time_stamp[i].day]

    def create_data_array(self) -> None:
        """
        From data attribute, create a data_np attribute (numpy array) in which
        each time series is associated to a specific channel number, component,
        channel name, location code, run number, start time and end time.
        """
        # Check for data gaps and day start
        time_stamp = self.data["Time_stamp"]
        ind_gaps = self.detect_gaps(time_stamp)
        ind_days = LemiData.detect_new_day(time_stamp)
        ind_traces = sorted(set([0, *ind_gaps, *ind_days, len(time_stamp)]))
        # Create structured numpy array
        npts_max = int(24 * 3600 * SAMPLING_RATE)
        dtype = [('cha_port', str, 2), ('comp', str, 2),
                 ('cha_name', str, 3), ('loc', str, 2),
                 ('run_num', int), ('starttime', UTCDateTime),
                 ('endtime', UTCDateTime), ('npts', int),
                 ('samples', float, npts_max)]
        self.data_np = np.zeros(len(DATA_TO_ARCHIVE) * (len(ind_traces) - 1), dtype=dtype)
        # Fill array for each time chunk and channel.
        # A time chunk is defined as a day or the time between two data gaps if
        # data recording is discontinuous.
        ind = 0
        run_num = 1
        loc = ''
        for start, end in zip(ind_traces[:-1], ind_traces[1:]):
            npts = end - start
            if start in ind_gaps:
                run_num += 1
            for cha in DATA_TO_ARCHIVE:
                cha_port = cha if cha.startswith('E') else ''
                comp = cha if not cha.startswith('E') else ''
                cha_name = CHA_NAMING_CONV.get(cha, '')
                samples = [*self.data[cha][start:end], *[0] * (npts_max - npts)]
                self.data_np[ind] = np.array([(cha_port, comp,
                                               cha_name, loc, run_num,
                                               time_stamp[start],
                                               time_stamp[end - 1], npts,
                                               samples)], dtype=dtype)
                ind += 1

    def update_stats_geo_time(self) -> None:
        """
        Update stats attribute by defining the latitude, longitude, elevation,
        start and end time for all channels.
        These stats info are used to set the header information for a ObsPy
        Trace object.
        """
        self.stats['lat'] = mean(self.data["Lat"])
        self.stats['lon'] = mean(self.data["Lon"])
        self.stats['elev'] = mean(self.data["Elev"])
        self.stats['start'] = self.data["Time_stamp"][0]
        self.stats['end'] = self.data["Time_stamp"][-1]

    def update_stats_net_sta(self, net: str, sta: str) -> None:
        """
        Update stats attribute by defining the network code and station name.
        These stats info are used to set the header information for a ObsPy
        Trace object.
        """
        self.stats['net'] = net.upper()
        self.stats['sta'] = sta.upper()

    def update_array(self, e_rec: Dict, e_infos: Dict) -> None:
        """
        Update data_np attribute based on:
        - list of electric channels for which data was recorded.
        - list of electrode pair info.
        """
        # Remove from data array, the electric channels for which data were not
        # recorded
        for run_id, record in e_rec.items():
            no_record = [x for x in VALID_E_COMPS if x not in record]
            self.update_e_rec(run_id, no_record)
        # Update component, channel_name and location code for electric channels
        bool_loc = eval_loc_code(e_infos)
        for run_id, e_info in e_infos.items():
            self.update_e_info(run_id, e_info, bool_loc)

    def get_ind_data(self, cha: str, run_num: int) -> np.ndarray:
        """Get data indexes for a given channel and run number."""
        ind_data = np.logical_and(self.data_np['cha_port'] == cha,
                                  self.data_np['run_num'] == run_num)
        return ind_data

    def update_e_rec(self, run_id: str, no_record: List) -> None:
        """Remove non-recorded electric channels from data_np attribute."""
        run_num = get_run_num(run_id)
        for cha in no_record:
            ind_data = self.get_ind_data(cha, run_num)
            self.data_np = self.data_np[np.invert(ind_data)]

    def update_e_info(self, run_id: str, e_info: Dict, bool_loc: bool) -> None:
        """
        Update electrode pair info (component, channel name and location code)
        in data_np attribute.
        """
        e_loc = get_e_loc(e_info) if bool_loc else {}
        run_num = get_run_num(run_id)
        for key, val in e_info.items():
            ind_data = self.get_ind_data(key, run_num)
            for ind in np.where(ind_data)[0]:
                self.data_np[ind]['comp'] = val
                self.data_np[ind]['cha_name'] = CHA_NAMING_CONV[val]
                self.data_np[ind]['loc'] = e_loc.get(key, '')

    @typing.no_type_check
    def update_data(self, qc_inputs: Optional[Dict] = None, md: Optional[LemiMetadata] = None) -> None:
        """
        Update stats and data_np attributes based on user inputs (entered
        either via the command line or the GUI).
        Called either right after parse_data_files() in QC mode or after
        parsing metadata info in normal mode.
        """
        if qc_inputs is not None:
            net = qc_inputs['net']
            sta = qc_inputs['sta']
            run_list = get_run_list(self.stats['num_runs'])
            e_infos = {run_id: qc_inputs['e_info'] for run_id in run_list}
            e_rec = {run_id: qc_inputs['e_chas'] for run_id in run_list}
        else:
            net = md.net.archive_net
            sta = md.sta.archive_id
            e_infos = md.get_e_infos()
            e_rec = {k: list(v.keys()) for k, v in e_infos.items()}
        self.update_stats_net_sta(net, sta)
        self.update_array(e_rec, e_infos)

    def prep_data(self):
        """Wrap up method to prep data outputted by LEMI-424."""
        self.parse_data_files()
        self.parse_inf_files()
        if not self.data:
            logger.error('No valid data found. Exiting!')
            sys.exit(1)
        else:
            self.create_data_array()
            self.update_stats_geo_time()
