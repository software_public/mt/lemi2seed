# -*- coding: utf-8 -*-
"""
Routines for parsing LEMI field spreadsheets(i.e. install and demobilization
sheets) and validating metadata;
Templates for the spreadsheets are provided to the PIs and their formatting is
hard coded.

Maeva Pourpoint - IRIS/PASSCAL
"""

from __future__ import annotations

import copy
import openpyxl
import pickle

from dataclasses import fields
from datetime import datetime
from inspect import signature
from obspy import UTCDateTime
from openpyxl.worksheet.worksheet import Worksheet
from operator import methodcaller
from pathlib import Path
from typing import Dict, List, Optional, TYPE_CHECKING, Union

from lemi2seed.lemi_data import CHA_NAMING_CONV
from lemi2seed.logging import parse_config_ini, setup_logger
from lemi2seed.metadata_category import Aux, Elec, Mag, Run, Sta, Net, MSG_MAP
from lemi2seed.utils import (is_empty, eval_loc_code, get_e_loc, get_e_ids,
                             get_run_list, str2list, NUM_E_CHA_MAX)

if TYPE_CHECKING:
    import numpy
    from lemi2seed.lemi_data import LemiData

# Read config.ini file and set up logging
config = parse_config_ini()
logger = setup_logger(__name__)

# -- Type aliases --
MD_VAL = Union[str, int, float, datetime, UTCDateTime]  # Type of metadata value
DCS = Union[Net, Sta, Run, Elec, Mag, Aux]  # Type of dataclasses
DCS_SUB_REMA = Union[Run, Elec, Mag, Aux]
DCS_SUB_EMA = Union[Elec, Mag, Aux]
DCS_SUB_MA = Union[Mag, Aux]
# -- Type aliases --

CHA_TYPES = {'elec': ['Ex', 'Ey'], 'mag': ['Hx', 'Hy', 'Hz'],
             'aux': ['Ui', 'Te', 'Tf', 'Sn', 'Fq', 'Ce']}


class LemiMetadata():
    """
    This class handles the parsing the LEMI field sheets, the population of the
    metadata and their validation.
    """

    def __init__(self, path2md: Path, output_xml: Path, lemi_data: LemiData) -> None:
        """Instantiate LemiMetadata class and scan metadata directory."""
        self.path2md = path2md
        self.output_xml = output_xml
        self.filenames: List[Path] = []
        self.cats: List[str] = ['Net', 'Sta', 'Run', 'Elec', 'Mag', 'Aux']
        self.net: Net = Net()
        self.sta: Sta = Sta()
        self.run: List[Run] = []
        self.elec: List[Elec] = []
        self.mag: List[Mag] = []
        self.aux: List[Aux] = []
        self.data_stats: Dict = lemi_data.stats
        self.data_np: numpy.ndarray = lemi_data.data_np
        self.run_list: List[str] = get_run_list(lemi_data.stats['num_runs'])
        if path2md is not None:
            self.scan_path2md()

    def scan_path2md(self) -> None:
        """
        Scan metadata directory. If no field sheets are found under the given
        directory, the user will have to provide the necessary metadata in the
        GUI.
        """
        filenames = [tmp for tmp in self.path2md.glob('*.xlsx')
                     if not tmp.parts[-1].startswith('~$')]
        if not filenames:
            logger.error("No field sheet found under the following path - {}. "
                         "All metadata will have to be provided using the "
                         "GUI!".format(self.path2md))
        else:
            self.filenames = filenames

    @staticmethod
    def get_md_field(sheet: Worksheet, cell: str) -> Optional[MD_VAL]:
        """Get metadata field at a given cell in a given field sheet."""
        try:
            field_val = sheet[cell].value
        except Exception as error:
            logger.exception("Failed to retrieve value of cell {} - Skipping "
                             "cell - Exception: {}".format(cell, error))
            return None
        return field_val

    def identify_field_sheet(self, sheet: Worksheet, filename: Path) -> Optional[str]:
        """
        Check whether a field sheet is an install, removal or credential sheet.
        Layout between provided spread sheet templates varies and cell numbers
        listed in config file are organized in three sub-categories:
        install_sheet, credential_sheet, removal_sheet.
        """
        sheet_header = LemiMetadata.get_md_field(sheet, config['sheets']['header'])
        sheet_types = {val: key for key, val in config['sheets'].items()[1:]}
        sheet_type = sheet_types.get(sheet_header)
        if sheet_type is None:
            logger.warning("The following file {} does not have the proper "
                           "header. The provided spread sheet templates were "
                           "not used or their layout was modified. Skipping "
                           "file!".format(filename))
        return sheet_type

    def init_run_props(self) -> List[Run]:
        """
        Instantiate Run data class object and initialize non-user defined
        properties.
        """
        runs = []
        run_num = [x['run_num'] for x in self.data_np]
        starttime = [x['starttime'] for x in self.data_np]
        endtime = [x['endtime'] for x in self.data_np]
        for ind_run, run_id in enumerate(self.run_list, 1):
            run = Run(resource_id=f'mt.run.id:{run_id}',
                      start=starttime[run_num.index(ind_run)],
                      end=endtime[len(run_num) - run_num[::-1].index(ind_run) - 1])
            runs.append(run)
        return runs

    def init_cha_props(self, cha_type: str, run_id: str) -> DCS_SUB_EMA:
        """
        Create instances of Elec, Mag or Aux data class and
        initialize properties inherited from the BaseSta and BaseChannel
        data classes.
        """
        ChaClass = globals()[cha_type.capitalize()]
        cha = ChaClass(elev=round(self.data_stats["elev"], 3),
                       lat=round(self.data_stats["lat"], 3),
                       lon=round(self.data_stats["lon"], 3),
                       run_id=run_id)
        return cha

    @staticmethod
    def dc2dict(dc: DCS_SUB_REMA) -> Dict:
        """
        Convert dataclass instance to a dict and filter dict to:
        - only keep metadata fields displayed in GUI
        - reset missing and invalid metadata fields
        """
        missing_invalid = dc.md_missing | dc.md_invalid
        mdict = {}
        for md_field in fields(dc):
            if md_field.metadata.get('gui'):
                if md_field.name not in missing_invalid:
                    mdict[md_field.name] = getattr(dc, md_field.name)
                else:
                    mdict[md_field.name] = md_field.default
        return mdict

    def filter_cha(self, cha_type: str, run_id: str) -> List[DCS_SUB_EMA]:
        """
        For a given channel type (electric, magnetic or auxiliary), get list of
        channels with a given run id.
        """
        return [c for c in getattr(self, cha_type) if c.run_id == run_id]

    @staticmethod
    def update_e_cha(cha: Elec, cha_port: str) -> Elec:
        """
        Make copy of populated Elec data classes and set channel port and
        electrode pair number.
        """
        # Use deepcopy here so that any changes made to the copy object do not
        # reflect in the original object.
        tmp = copy.deepcopy(cha)
        tmp.cha_port = cha_port
        tmp.pair_num = int(cha_port[1])
        return tmp

    @property
    def for_gui(self) -> Dict:
        """
        Define metadata output dictionary structure that will be used to set
        the model in the GUI.
        """
        output: Dict = {}
        for cat in self.cats[:-1]:
            output[cat] = {}
            mprop = getattr(self, cat.lower())
            if isinstance(mprop, list):
                if cat == 'Run':
                    if not mprop:
                        mprop = self.init_run_props()
                    for ind, dc in enumerate(mprop):
                        key = f'Run_{self.run_list[ind]}'
                        output[cat][key] = LemiMetadata.dc2dict(dc)
                elif cat == 'Elec':
                    for run_id in self.run_list:
                        chas = self.filter_cha('elec', run_id)
                        if not chas:
                            cha = self.init_cha_props('elec', run_id)
                            chas = [LemiMetadata.update_e_cha(cha, f'E{ind + 1}')  # type: ignore
                                    for ind in range(NUM_E_CHA_MAX)]
                        for ind in range(NUM_E_CHA_MAX):
                            key = f'Run_{run_id}_Elec_Pair_{ind+1}'
                            output[cat][key] = LemiMetadata.dc2dict(chas[ind])
                elif cat == 'Mag':
                    for run_id in self.run_list:
                        chas = self.filter_cha('mag', run_id)
                        if not chas:
                            dc = self.init_cha_props('mag', run_id)
                        else:
                            dc = chas[0]
                        key = f'Run_{run_id}_Mag'
                        output[cat][key] = LemiMetadata.dc2dict(dc)
            else:
                output[cat] = LemiMetadata.dc2dict(mprop)
        return output

    def parse_field_sheet(self, filename: Path, sheet: Worksheet, sheet_type: str, md_fields: Dict) -> Dict:
        """
        Parse field sheet using the cell numbers listed in the config file and
        return a nested dictionary of metadata entries organized based on the
        category under which a given metadata field falls.
        """
        logger.info("Parsing the {} {} - {}.".format(*sheet_type.split('_'), filename))
        for cat in self.cats[:-1]:
            if cat not in md_fields:
                md_fields[cat] = {}
            if sheet_type not in config[cat]:
                continue
            logger.info("Parsing metadata fields at the {} level.".format(MSG_MAP[cat]))
            for key, val in config[cat][sheet_type].items():
                if isinstance(val, list):
                    field_val = [LemiMetadata.get_md_field(sheet, x) for x in val]
                else:
                    field_val = LemiMetadata.get_md_field(sheet, val)  # type: ignore
                if key == "comments":
                    field_val = ', '.join(filter(None, (md_fields[cat].get(key, None), field_val)))  # type: ignore
                md_fields[cat][key] = field_val
        return md_fields

    def reformat_run(self, run_fields: Dict) -> Dict:
        """Reformat run metadata fields."""
        tmp = {f'Run_{run_id}': dict.fromkeys(run_fields.keys()) for run_id in self.run_list}
        tmp['Run_a'].update(run_fields)
        return tmp

    def reformat_elec(self, elec_fields: Dict) -> Dict:
        """Reformat electric metadata fields."""
        e_ids = get_e_ids(self.data_stats['num_runs'])
        tmp = {f'Run_{run_id}_Elec_Pair_{ind}': dict.fromkeys(elec_fields.keys())
               for run_id, ind in e_ids}
        for key, val in elec_fields.items():
            for ind in range(1, NUM_E_CHA_MAX + 1):
                if isinstance(val, list):
                    e_val = val[ind - 1]
                else:
                    e_val = val
                tmp[f'Run_a_Elec_Pair_{ind}'][key] = e_val
        return tmp

    def reformat_mag(self, mag_fields: Dict) -> Dict:
        """Reformat magnetic metadata fields."""
        tmp = {f'Run_{run_id}_Mag': dict.fromkeys(mag_fields.keys())
               for run_id in self.run_list}
        tmp['Run_a_Mag'].update(mag_fields)
        return tmp

    def reformat_md_dict(self, md_fields: Dict) -> Dict:
        """
        Reformat metadata_fields to include all runs and associate correct
        metadata info to each electrode pair.
        """
        md_fields['Run'] = self.reformat_run(md_fields['Run'])
        md_fields['Elec'] = self.reformat_elec(md_fields['Elec'])
        md_fields['Mag'] = self.reformat_mag(md_fields['Mag'])
        return md_fields

    @staticmethod
    def check_field_sheets(sheet_types: List[Optional[str]]) -> None:
        """
        Check that all valid sheet types have been found and parsed. If not,
        warn user of missing sheet(s).
        """
        valid_types = [key for key in config['sheets'].keys()[1:]]
        for valid_type in valid_types:
            if sheet_types and valid_type not in sheet_types:
                logger.warning("No valid {0} sheet found. The {0} metadata "
                               "fields will have to be provided using the GUI."
                               .format(valid_type.split('_')[0]))

    @property
    def from_field_sheets(self) -> Optional[Dict]:
        """Parse all field sheets provided by the user."""
        md_fields: Dict = {}
        sheet_types = []
        parsed_sheets = []
        for filename in self.filenames:
            try:
                workbook = openpyxl.load_workbook(filename, data_only=True)  # type: ignore
            except Exception as error:
                logger.error("Failed to open field sheet: {} - Skipping file "
                             "- Exception: {}".format(filename, error))
            else:
                if workbook is None:
                    continue
                sheet = workbook.active
                sheet_type = self.identify_field_sheet(sheet, filename)
                sheet_types.append(sheet_type)
                if sheet_type is None:
                    continue
                if sheet_type not in parsed_sheets:
                    parsed_sheets.append(sheet_type)
                else:
                    logger.warning("Already parsed the {0} {1} - You may have "
                                   "more than one {0} {1} - Skipping {2}!"
                                   .format(*sheet_type.split('_'), filename))
                    continue
                md_fields = self.parse_field_sheet(filename, sheet, sheet_type, md_fields)
                workbook.close()
        LemiMetadata.check_field_sheets(sheet_types)
        return self.reformat_md_dict(md_fields) if not is_empty(md_fields) else None

    def populate(self, cat: DCS, md_fields: Dict, run_id: Optional[str] = None) -> None:
        """
        Populate metadata properties for given "category" of dataclass.
        Loop over all metadata fields for given "category" and call appropriate
        validation method based on metadata fields name.
        If validation method exists, use method to validate metadata field value.
        Some metadata inputs are validated against data inputs. Ex: latitude,
        longitude, elevation, serial numbers, dipole length ...
        """
        for key, val in md_fields.items():
            valid = True
            validate_method = f'validate_{key}'
            if isinstance(val, datetime):
                val = UTCDateTime(val)
            if hasattr(cat, validate_method):
                if "data_input" in signature(getattr(cat, validate_method)).parameters:
                    data_input = self.data_stats[key]
                    if isinstance(data_input, dict):
                        data_input = self.data_stats[key][run_id]
                    valid = methodcaller(validate_method, val, data_input)(cat)
                else:
                    valid = methodcaller(validate_method, val)(cat)
            if valid:
                setattr(cat, key, val)
                cat.md_invalid.discard(key)
            else:
                cat.md_invalid.add(key)

    @staticmethod
    def flag_md_missing(cat: DCS, skip: List[str] = []) -> None:
        """
        Flag metadata fields that are required for archiving but "missing"
        because not provided by the user.
        """
        md_props = cat.__dict__
        md_req = [md_field.name for md_field in fields(cat)
                  if md_field.metadata.get('req')]
        for key, val in md_props.items():
            if key in md_req and key not in skip:
                if is_empty(val):
                    cat.md_missing.add(key)
                else:
                    cat.md_missing.discard(key)

    def populate_net_props(self, md_fields_n: Dict) -> None:
        """
        Populate properties of Network data class based on user inputs from
        the field sheets and/or GUI.
        """
        self.populate(self.net, md_fields_n)
        LemiMetadata.flag_md_missing(self.net)

    def check_sta_start_end(self) -> None:
        """
        Check that the start and end times of a station fall within the
        network start and end times.
        """
        for time_field in ['start', 'end']:
            time_net = getattr(self.net, time_field)
            if time_field in self.sta.md_invalid or time_net is None:
                continue
            time_sta = getattr(self.sta, time_field)
            valid = time_sta >= time_net if time_field == "start" else time_sta <= time_net
            if not valid:
                msg = "greater" if time_field == "start" else "lower"
                logger.error("The {0} date of the station should be {1} than the {0} date "
                             "of the network.".format(time_field, msg))
                self.sta.md_invalid.add(time_field)

    def populate_sta_props(self, md_fields_s: Dict) -> None:
        """
        Populate properties of Sta data class based on user inputs from
        the field sheets and/or GUI.
        """
        self.populate(self.sta, md_fields_s)
        self.sta.run_list = ', '.join(self.run_list)
        self.check_sta_start_end()
        LemiMetadata.flag_md_missing(self.sta)

    def get_run(self, run_id: str) -> Run:
        """Get run data class for given run id."""
        ind_run = self.run_list.index(run_id)
        return self.run[ind_run]

    def populate_run_props(self, md_fields_r: Dict, run_list: Optional[List] = None) -> None:
        """
        Populate properties of run data class based on user inputs from
        the field sheets and/or GUI.
        """
        run_list_ = self.run_list if run_list is None else run_list
        if not self.run:
            self.run.extend(self.init_run_props())
        for run_id in run_list_:
            run = self.get_run(run_id)
            name = f'Run_{run_id}'
            self.populate(run, md_fields_r[name], run_id)
            LemiMetadata.flag_md_missing(run)

    def get_comps_rec(self, type_: str, run_id: str) -> list:
        """
        For a given channel type (electric or magnetic) and a given run_id,
        get components recorded.
        """
        run = self.get_run(run_id)
        if run.comps_rec is None:
            return []
        else:
            comps = [x for x in str2list(run.comps_rec, sep='-') if x.startswith(type_)]  # type: ignore
            return comps

    def match_num_e_pairs(self, e_pairs: List[str], run_id: str, num_e_pairs: int) -> None:
        """
        Check that number of electrode pairs specified at the Elec level for a
        given run matches the number of electric channels specified by the user
        at the run level.
        """
        e_chas = self.filter_cha('elec', run_id)
        if num_e_pairs != len(e_pairs):
            logger.error("Invalid number of electrode pairs (run '{}')! "
                         "The number of electrode pairs does not match the "
                         "number of electric channels in your list of recorded "
                         "components at the station/run level.".format(run_id))
            for e_cha in e_chas:
                e_cha.md_invalid.add(f'run_{run_id}_num_e_pairs')
        else:
            for e_cha in e_chas:
                e_cha.md_invalid.discard(f'run_{run_id}_num_e_pairs')

    def get_e_infos(self) -> Dict:
        """
        Get electrode pair information (channel port and associated component)
        for each run.
        """
        e_infos = {}
        for run in self.run:
            run_id = run.run_id
            e_pairs = self.get_comps_rec('E', run_id)
            elec = self.filter_cha('elec', run_id)
            e_infos[run_id] = {x.cha_port: x.comp for x in elec if f'E{x.pair_num}' in e_pairs}  # type: ignore
        return e_infos

    def update_loc(self):
        """
        Update location code for the electric channel if more than two
        electrode pairs were deployed at a station at a given point.
        """
        e_infos = self.get_e_infos()
        bool_loc = eval_loc_code(e_infos)
        for run_id, e_info in e_infos.items():
            elec = self.filter_cha('elec', run_id)
            e_loc = get_e_loc(e_info) if bool_loc else {}
            for key in e_info.keys():
                elec_ = [c for c in elec if c.cha_port == key]
                elec_[0].loc_code = e_loc.get(key, '')

    def populate_elec_props(self, md_fields_e: Dict, run_list: Optional[List] = None,
                            num_e_pairs: Optional[Dict] = None) -> Dict:
        """
        Populate properties of Elec data class based on user inputs from
        the field sheets and/or GUI.
        """
        num_e_pairs_ = {}
        run_list_ = self.run_list if run_list is None else run_list
        for run_id in run_list_:
            e_pairs = self.get_comps_rec('E', run_id)
            if not e_pairs:
                continue
            if not self.filter_cha('elec', run_id):
                cha = self.init_cha_props('elec', run_id)
                self.elec.extend([LemiMetadata.update_e_cha(cha, f'E{i+1}')  # type: ignore
                                  for i in range(NUM_E_CHA_MAX)])
            num_e_pairs_[run_id] = (int(num_e_pairs.get(run_id) or len(e_pairs))
                                    if num_e_pairs else len(e_pairs))
            set_methods = [x for x in dir(Elec) if x.startswith('set_')]
            for ind, cha in enumerate(self.filter_cha('elec', run_id)):
                key = f'Run_{run_id}_Elec_Pair_{ind+1}'
                if ind in range(num_e_pairs_[run_id]):
                    self.populate(cha, md_fields_e[key], run_id)
                    for set_method in set_methods:
                        methodcaller(set_method)(cha)
                    LemiMetadata.flag_md_missing(cha)
                else:
                    cha.cha_port = md_fields_e[key]['cha_port']  # type: ignore
            self.match_num_e_pairs(e_pairs, run_id, num_e_pairs_[run_id])
        self.update_loc()
        return num_e_pairs_

    def get_cha_inds(self, cha_type: str, run_id: str) -> List[int]:
        """
        For a given channel type (electric, magnetic or auxiliary), get indexes
        of channels with a given run id.
        """
        return [i for i, c in enumerate(getattr(self, cha_type)) if c.run_id == run_id]

    @staticmethod
    def update_comp_cha_name(cha: DCS_SUB_MA, comp: str) -> DCS_SUB_MA:
        """
        Make copy of populated Magnetic or Aux data classes and set component
        and channel name.
        """
        # Use deepcopy here so that any changes made to the copy object do not
        # reflect in the original object.
        tmp = copy.deepcopy(cha)
        tmp.comp = comp
        tmp.cha_name = CHA_NAMING_CONV[tmp.comp]
        return tmp

    def populate_mag_props(self, md_fields_m: Dict, run_list: Optional[List] = None) -> None:
        """
        Populate properties of Mag data class based on user inputs from the
        field sheets and/or GUI.
        For now, we are assuming that a 3-component magnetometer was installed.
        """
        run_list_ = self.run_list if run_list is None else run_list
        for run_id in run_list_:
            m_comps = self.get_comps_rec('H', run_id)
            if not m_comps:
                continue
            if not self.filter_cha('mag', run_id):
                cha = self.init_cha_props('mag', run_id)
                # Use deepcopy here so that any changes made to the copy object
                # do not reflect in the original object.
                self.mag.extend([copy.deepcopy(cha) for i in range(len(CHA_TYPES['mag']))])  # type: ignore
            cha_inds = self.get_cha_inds('mag', run_id)
            cha = self.mag[cha_inds[0]]
            name = f'Run_{run_id}_Mag'
            self.populate(cha, md_fields_m[name], run_id)
            for set_method in [x for x in dir(Mag) if x.startswith('set_')]:
                methodcaller(set_method)(cha)
            LemiMetadata.flag_md_missing(cha, skip=['comp', 'cha_name'])
            for ind, cha_ind in enumerate(cha_inds):
                comp = CHA_TYPES['mag'][ind]
                self.mag[cha_ind] = LemiMetadata.update_comp_cha_name(cha, comp)  # type: ignore

    def populate_aux_props(self) -> None:
        """
        Populate properties of Aux data class based on user inputs from
        the field sheets and/or GUI.
        """
        for run_id in self.run_list:
            if not self.filter_cha('aux', run_id):
                cha = self.init_cha_props('aux', run_id)
                # Use deepcopy here so that any changes made to the copy object
                # do not reflect in the original object.
                self.aux.extend([copy.deepcopy(cha) for i in range(len(CHA_TYPES['aux']))])  # type: ignore
            cha_inds = self.get_cha_inds('aux', run_id)
            cha = self.aux[cha_inds[0]]
            cha.sn = self.data_stats['datalogger_sn'][run_id]
            for set_method in [x for x in dir(Aux) if x.startswith('set_')]:
                methodcaller(set_method)(cha)
            LemiMetadata.flag_md_missing(cha, skip=['comp', 'cha_name'])
            for ind, cha_ind in enumerate(cha_inds):
                comp = CHA_TYPES['aux'][ind]
                self.aux[cha_ind] = LemiMetadata.update_comp_cha_name(cha, comp)  # type: ignore

    def populate_props(self, md_fields: Dict) -> None:
        """
        Populate metadata properties based on user inputs from the field sheets
        and/or GUI.
        """
        for cat in self.cats:
            md_fields_ = md_fields.get(cat)
            populate_method = f'populate_{cat.lower()}_props'
            if md_fields_:
                methodcaller(populate_method, md_fields_)(self)
            else:
                methodcaller(populate_method)(self)

    def save_md(self, filename: str) -> None:
        """
        Save instance of LemiMetadata class into a byte stream.
        Useful if:
        - the user wants to update metadata fields and regenerate StationXML
          files after terminating lemi2seed.
        - the user wants to reuse some of the metadata fields for another
          station.
        - lemi2seed is terminated unexpectedly.
        """
        with open(filename, 'wb') as fout:
            logger.info("Saving metadata inputs in {}".format(filename))
            pickle.dump(self, fout)

    @staticmethod
    def load_md(filename: str) -> LemiMetadata:
        """Load saved instance of LemiMetadata class."""
        with open(filename, 'rb') as fin:
            logger.info("Loading metadata inputs from {}".format(filename))
            lemi_md = pickle.load(fin)
        return lemi_md

    def update_azimuth_tilt(self) -> None:
        """
        Update azimuth and tilt for magnetic field channels.
        LEMI-039 fluxgate is a 3-component magnetometer. So by convention:
        - the tilt for the Hx and Hy channels should be set to 0°
        - the azimuth of the Hx and Hy channels should be 90° offset
        - the azimuth for the Hz channel should be set to 0°.
        """
        for run_id in self.run_list:
            chas = self.filter_cha('mag', run_id)
            for cha in chas:
                if cha.comp in ['Hx', 'Hy']:
                    cha.meas_tilt = 0.0
                    if cha.comp == 'Hy':
                        cha.meas_azimuth = float(cha.meas_azimuth) + 90.0  # type: ignore
                else:
                    tilt = float(cha.meas_tilt)  # type: ignore
                    if tilt > 0.0:
                        cha.meas_tilt = -tilt  # to match SEED convention
                    cha.meas_azimuth = 0.0
