# -*- coding: utf-8 -*-
"""
Routines that handle LEMI instrument response.

-----
LEMI calibration information

The LEMI manufacturer provided PASSCAL with a calibration file for each
component of a logger/sensor pair.

Example:
- LEMI-424_N131_Bx.rsp
- LEMI-424_N131_By.rsp
- LEMI-424_N131_Bz.rsp
- LEMI-424_N131_E1.rsp
- LEMI-424_N131_E2.rsp
- LEMI-424_N131_E3.rsp
- LEMI-424_N131_E4.rsp

The first three files being the calibration files for the magnetic channels.
The latter files being the calibration files for the electric channels.
N131 refering to the serial number of the data logger and sensor.

Each data logger and sensor are a matched pair and should be deployed together.
Because, each sensor (magnetometer or electrode) was calibrated with its
matching data logger, each calibration file provided by LEMI include the joint
response of logger/sensor pair (convolution of two filters).

The LEMI logger "internally corrects" the recorded signal and outputs physical
units (nT for the Hx channel and µV/m for the electric channels). Thus, after
converting the calibration files from a FAP to ZPK format (using the
lemi_FAP_to_PZs jupyter notebook), we use the poles and zeros
representation of the logger/sensor pair filter to create a single stage
response (handled in the PZ_to_ObspyResponse jupyter notebook).The generated
single stage response would be equivalent to a frequency-dependent correction
factor that one would have to apply to the data to account/correct for some
assumptions made by the LEMI manufacturers that don't hold as we get closer
to Nyquist (i.e., unity gain and zero phase).

The single stage responses for all logger/sensor pairs and channels (ObsPy
Response objects) are stored in a pickled dictionary and packaged with the
lemi2seed application (./response_file/LEMI-424_Response).

A coefficient multiplier filter is used to represent the SOH channels' filter.

-----

Maeva Pourpoint - IRIS/PASSCAL
"""

from __future__ import annotations

import pickle
import sys

from obspy.core.inventory import Response, CoefficientsTypeResponseStage, InstrumentSensitivity
from pathlib import Path
from pickle import UnpicklingError
from typing import Dict

from lemi2seed.logging import setup_logger

# Set up logging
logger = setup_logger(__name__)

RESP_FILE = Path(__file__).resolve().parent.joinpath('response_file', 'LEMI-424_Response')
SOH_STAGE_GAIN = 1.0
SOH_STAGE_GAIN_FREQUENCY = 0.0
CHA_UNITS = {'E1': ['uV/m', 'electric field units'],
             'E2': ['uV/m', 'electric field units'],
             'E3': ['uV/m', 'electric field units'],
             'E4': ['uV/m', 'electric field units'],
             'Hx': ['nanotesla', 'magnetic field units'],
             'Hy': ['nanotesla', 'magnetic field units'],
             'Hz': ['nanotesla', 'magnetic field units'],
             'Ui': ['V', 'electric potential in volts'],
             'Te': ['celsius', 'temperature in degrees Celsius'],
             'Tf': ['celsius', 'temperature in degrees Celsius'],
             'Sn': ['unitless', 'unitless'],
             'Fq': ['unitless', 'unitless'],
             'Ce': ['s', 'second']}


class LemiResponse():
    """
    This class handles the creation of a single stage response for each data or
    state of health channel.
    """

    def __init__(self, sn: str, cha_code: str, sample_rate: float) -> None:
        """
        Instantiate LemiResponse class and load response file for all
        logger/sensor pairs and channels.
        """
        self.sn = f"N{sn}"
        self.cha_code = cha_code
        self.sample_rate = sample_rate

    @staticmethod
    def check_resp_file() -> None:
        """
        Check that the response file exists under the expected directory.
        Account for the unlikely scenario that the user moved or renamed the
        response file.
        """
        if not RESP_FILE.is_file():
            logger.error("No response file '{}' found under the following directory: {}. "
                         "Exiting ...".format(RESP_FILE.name, RESP_FILE.parent))
            sys.exit(1)

    @staticmethod
    def load_resp() -> Dict:
        """
        Load dictionary containing the single stage responses for all
        logger/sensor pairs and channels.
        """
        with open(RESP_FILE, 'rb') as fin:
            try:
                return pickle.load(fin)
            except UnpicklingError:
                logger.error("Failed to 'unpickled' response file {}. The "
                             "file may have been corrupted. Exiting ..."
                             .format(RESP_FILE.name))
                sys.exit(1)

    def create_coefficient_filter(self) -> CoefficientsTypeResponseStage:
        """
        Create a coefficient multiplier filter (frequency independant gain).
        A coefficient multiplier filter can be represented as a FIR filter with a
        single coefficient (i.e. number of numerators is 1 and denominators is 0).
        When no denominators are provided, the filter must be digital.
        """
        units = CHA_UNITS[self.cha_code]
        cf_filter = CoefficientsTypeResponseStage(stage_sequence_number=1,
                                                  stage_gain=SOH_STAGE_GAIN,
                                                  stage_gain_frequency=SOH_STAGE_GAIN_FREQUENCY,
                                                  input_units=units[0],
                                                  output_units=units[0],
                                                  cf_transfer_function_type='DIGITAL',
                                                  decimation_input_sample_rate=self.sample_rate,
                                                  decimation_factor=1,
                                                  decimation_offset=0,
                                                  decimation_delay=0.0,
                                                  decimation_correction=0.0,
                                                  numerator=[1],
                                                  denominator=[],
                                                  description="coefficient multiplier filter",
                                                  input_units_description=units[1],
                                                  output_units_description=units[1])
        return cf_filter

    def get_data_cha_resp(self, resps: Dict) -> Response:
        """
        Get single stage channel response (PolesZerosResponseStage) for given
        data channel.
        """
        sn_cha = f"{self.sn}_{self.cha_code}"
        matching_keys = sorted([key for key in resps.keys() if sn_cha in key])
        if not matching_keys:
            logger.error("No instrument response found for given channel '{}' "
                         "and serial number '{}'. Exiting ..."
                         .format(self.cha_code, self.sn))
            sys.exit(1)
        data_cha_resp = resps[matching_keys[-1]]
        return data_cha_resp

    def get_soh_cha_resp(self) -> Response:
        """
        Create a single stage response (CoefficientsTypeResponseStage) for the
        state of health channels.
        """
        units = CHA_UNITS[self.cha_code]
        soh_cha_resp = Response()
        soh_cha_resp.instrument_sensitivity = InstrumentSensitivity(value=SOH_STAGE_GAIN,
                                                                    frequency=SOH_STAGE_GAIN_FREQUENCY,
                                                                    input_units=units[0],
                                                                    output_units=units[0],
                                                                    input_units_description=units[1],
                                                                    output_units_description=units[1])
        cf_filter = self.create_coefficient_filter()
        soh_cha_resp.response_stages = [cf_filter]
        return soh_cha_resp
