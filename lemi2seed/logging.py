# -*- coding: utf-8 -*-
"""
Module that handles:
- setting up the logger
- parsing the config.ini file

Logging outputs are printed out on the console and saved to the lemi2seed.msg
file.

The config.ini file includes the cell coordinates necessary to parse the field
spread sheets.

Maeva Pourpoint - IRIS/PASSCAL
"""

import logging
import logging.config
import sys

from configobj import ConfigObj
from logging import Logger
from pathlib import Path


def parse_config_ini() -> ConfigObj:
    config_file = Path(__file__).resolve().parent.joinpath('config', 'config.ini')
    config = ConfigObj(str(config_file))
    return config


def setup_logger(module_name: str) -> Logger:
    # Path to logging config file
    log_file_path = Path(__file__).resolve().parent.joinpath('config', 'logging.conf')
    # Handle warnings from obspy.core.inventory modules with standard logging
    logging.captureWarnings(True)
    # Read in logging config file
    logging.config.fileConfig(log_file_path)
    # Create logger
    logger = logging.getLogger(module_name)
    return logger


def initiate_logger(logger: Logger, program_version: str) -> None:
    logger.info('lemi2seed program starting')
    logger.debug('%s - v%s - Python:%s' % (sys.argv, program_version, sys.version))
    logger.debug('CWD: %s' % Path.cwd())
    logger.info('Version: ' + program_version)
