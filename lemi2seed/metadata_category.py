# -*- coding: utf-8 -*-
"""
The metadata for a complete  MT dataset is structured into the following
categories: Net, Sta, Run, Elec, Mag, and Aux.

This module defines these categories as dataclasses with specific list of
attributes and validation methods.

Maeva Pourpoint - IRIS/PASSCAL
"""

from __future__ import annotations

import re

from dataclasses import dataclass, field
from obspy import UTCDateTime
from typing import Dict, Optional, Set

from lemi2seed.lemi_data import SAMPLING_RATE, CHA_NAMING_CONV
from lemi2seed.logging import parse_config_ini, setup_logger
from lemi2seed.utils import (check_inst_specs, check_sn, str2list,
                             ELEC_DEFAULT, FLUXGATE_DEFAULT)

# Read config.ini file and set up logging
config = parse_config_ini()
logger = setup_logger(__name__)

# Tolerance value (in seconds per sample) used as a threshold for time error
# detection in data from the channel
CLOCKDRIFT = 1.0
E_COMP_NAMING_CONV = {'North-South': 'Ex', 'South-North': 'Ex',
                      'East-West': 'Ey', 'West-East': 'Ey'}
MAXIMUM_GEO = 99999
MINIMUM_GEO = -99999
MSG_MAP = {'Net': 'network', 'Sta': 'station', 'Run': 'run', 'Elec': 'electric',
           'Mag': 'magnetic'}
VALID_COMPS = ['Hx - Hy - Hz', 'E1 - E2 - Hx - Hy - Hz', 'E1 - E2 - E3 - E4 - Hx - Hy - Hz']


@dataclass
class BaseMD:
    """
    This class handles the metadata attributes shared at all levels: network;
    station; run; electric; magnetic; auxiliary.
    """
    # List of metadata fields that are required for archiving but "missing"
    # because not provided by the user.
    md_missing: Set[str] = field(default_factory=set)
    # List of metadata fields that are invalid
    md_invalid: Set[str] = field(default_factory=set)


@dataclass
class BaseNet(BaseMD):
    """
    This class handles the metadata attributes shared at the network and station
    levels.
    """
    end: Optional[UTCDateTime] = field(default=None,
                                       metadata={'xml_id': ['end_date'],
                                                 'req': True, 'gui': True})
    start: Optional[UTCDateTime] = field(default=None,
                                         metadata={'xml_id': ['start_date'],
                                                   'req': True, 'gui': True})

    def validate_end(self, md_input: UTCDateTime, data_input: UTCDateTime) -> bool:
        if isinstance(md_input, UTCDateTime) and md_input >= data_input:
            return True
        else:
            logger.error("The end date of the {} should be in UTC and greater "
                         "than the data acquisition end time."
                         .format(MSG_MAP[self.__class__.__name__.split('_')[0]]))
            return False

    def validate_start(self, md_input: UTCDateTime, data_input: UTCDateTime) -> bool:
        if isinstance(md_input, UTCDateTime) and md_input <= data_input:
            return True
        else:
            logger.error("The start date of the {} should be in UTC and lower "
                         "than the data acquisition start time."
                         .format(MSG_MAP[self.__class__.__name__.split('_')[0]]))
            return False


@dataclass
class Net(BaseNet):
    """This class handles all the metadata attributes at the network level."""
    acq_by_author: Optional[str] = field(default=None,
                                         metadata={'xml_id': ['comments', 'mt.network.acquired_by.author'],
                                                   'req': False, 'gui': True})
    acq_by_comments: Optional[str] = field(default=None,
                                           metadata={'xml_id': ['comments', 'mt.network.acquired_by.comments'],
                                                     'req': False, 'gui': True})
    archive_net: Optional[str] = field(default=None,
                                       metadata={'req': True, 'gui': True})
    comments: Optional[str] = field(default=None,
                                    metadata={'xml_id': ['comments', 'mt.network.comments'],
                                              'req': False, 'gui': True})
    country: Optional[str] = field(default=None,
                                   metadata={'xml_id': ['comments', 'mt.network.country'],
                                             'req': False, 'gui': True})
    geo_name: Optional[str] = field(default=None,
                                    metadata={'xml_id': ['comments', 'mt.network.geographic_name'],
                                              'req': True, 'gui': True})
    name: Optional[str] = field(default=None,
                                metadata={'xml_id': ['description'],
                                          'req': True, 'gui': True})
    project: Optional[str] = field(default=None,
                                   metadata={'xml_id': ['comments', 'mt.network.project'],
                                             'req': True, 'gui': True})
    project_lead_author: Optional[str] = field(default=None,
                                               metadata={'xml_id': ['operators.contacts.names'],
                                                         'req': False, 'gui': True})
    project_lead_organization: Optional[str] = field(default=None,
                                                     metadata={'xml_id': ['operators.agency'],
                                                               'req': False, 'gui': True})
    summary: Optional[str] = field(default=None,
                                   metadata={'xml_id': ['comments', 'mt.network.summary'],
                                             'req': False, 'gui': True})

    @staticmethod
    def validate_archive_net(md_input: str) -> bool:
        try:
            valid = re.match(r"^\w{2}$", md_input)
        except TypeError:
            logger.error("The network code should be a string.")
            return False
        else:
            if valid is None:
                logger.error("The network code should be two alphanumeric "
                             "character long.")
            return bool(valid)


@dataclass
class BaseSta(BaseMD):
    """
    This class handles the metadata attributes shared at the station and
    channel levels.
    """
    data_type: str = field(default='LPMT',
                           metadata={'xml_id': ['comments', 'mt.station.data_type'],
                                     'req': True, 'gui': False})
    elev: Optional[float] = field(default=None,
                                  metadata={'req': True, 'gui': True})
    lat: Optional[float] = field(default=None,
                                 metadata={'req': True, 'gui': True})
    lon: Optional[float] = field(default=None,
                                 metadata={'req': True, 'gui': True})

    @staticmethod
    def validate_geos(param: tuple, md_input: float) -> bool:
        geo, min_range, max_range, min_val, max_val, val, units = param
        try:
            md_input = float(md_input)
        except (TypeError, ValueError):
            logger.error("{} should be a float.".format(geo.capitalize()))
            return False
        else:
            if md_input < min_range or md_input > max_range:
                logger.error("Unexpected {0}! The {0} should be between {1}"
                             "{3} and {2}{3}.".format(geo, min_range,
                                                      max_range, units))
                return False
            if md_input < min_val or md_input > max_val:
                tolerance = max_val - val
                logger.warning("Unexpected {0}! Provided {0} should "
                               "roughly match the {0} recorded by the on-site "
                               "GPS ({1:.3f} ± {2:.3f}{3})."
                               .format(geo, val, tolerance, units))
            return True

    def validate_elev(self, md_input: float, data_input: float) -> bool:
        min_elev, max_elev = [data_input + x for x in [-100, 100]]
        param = ("station elevation", -500, 8500, min_elev, max_elev, data_input, 'm')
        return BaseSta.validate_geos(param, md_input)

    def validate_lat(self, md_input: float, data_input: float) -> bool:
        min_lat, max_lat = [data_input + x for x in [-0.005, 0.005]]
        param = ("station latitude", -90, 90, min_lat, max_lat, data_input, '°')
        return BaseSta.validate_geos(param, md_input)

    def validate_lon(self, md_input: float, data_input: float) -> bool:
        min_lon, max_lon = [data_input + x for x in [-0.005, 0.005]]
        param = ("station longitude", -180, 180, min_lon, max_lon, data_input, '°')
        return BaseSta.validate_geos(param, md_input)


@dataclass
class Sta(BaseNet, BaseSta):
    """This class handles all the metadata attributes at the station level."""
    archive_id: Optional[str] = field(default=None,
                                      metadata={'req': True, 'gui': True})
    comments: Optional[str] = field(default=None,
                                    metadata={'xml_id': ['comments', 'mt.station.comments'],
                                              'req': False, 'gui': True})
    declination_model: Optional[str] = field(default=None,
                                             metadata={'xml_id': ['comments', 'mt.station.location.declination.model'],
                                                       'req': True, 'gui': True})
    declination_val: Optional[float] = field(default=None,
                                             metadata={'xml_id': ['comments', 'mt.station.location.declination.value'],
                                                       'req': True, 'gui': True})
    geo_name: Optional[str] = field(default=None,
                                    metadata={'xml_id': ['site'],
                                              'req': True, 'gui': True})
    orientation_method: Optional[str] = field(default=None,
                                              metadata={'xml_id': ['comments', 'mt.station.orientation.method'],
                                                        'req': False, 'gui': True})
    orientation_reference_frame: Optional[str] = field(default=None,
                                                       metadata={'xml_id': ['comments', 'mt.station.orientation.reference_frame'],  # noqa: E501
                                                                 'req': True, 'gui': True})
    run_list: str = field(default='a',
                          metadata={'xml_id': ['comments', 'mt.station.run_list'],
                                    'req': True, 'gui': False})
    software_author: str = field(default=config['software']['author'],
                                 metadata={'xml_id': ['comments', 'mt.station.provenance.software.author'],
                                           'req': False, 'gui': False})
    software_name: str = field(default=config['software']['name'],
                               metadata={'xml_id': ['comments', 'mt.station.provenance.software.name'],
                                         'req': False, 'gui': False})
    software_version: str = field(default=config['software']['version'],
                                  metadata={'xml_id': ['comments', 'mt.station.provenance.software.version'],
                                            'req': False, 'gui': False})
    submitter_author: Optional[str] = field(default=None,
                                            metadata={'xml_id': ['comments', 'mt.station.provenance.submitter.author'],
                                                      'req': False, 'gui': True})
    submitter_organization: Optional[str] = field(default=None,
                                                  metadata={'xml_id': ['comments', 'mt.station.provenance.submitter.organization'],  # noqa: E501
                                                            'req': False, 'gui': True})

    @staticmethod
    def validate_archive_id(md_input: str) -> bool:
        try:
            valid = re.match(r"^\w{3,5}$", md_input)
        except TypeError:
            logger.error("The station name should be a string.")
            return False
        else:
            if valid is None:
                logger.error("The station name should be between three and "
                             "five alphanumeric character long.")
            return bool(valid)

    def validate_declination_val(self, md_input: float) -> bool:
        param = ("declination angle", -90, 90, MINIMUM_GEO, MAXIMUM_GEO, None, '°')
        return BaseSta.validate_geos(param, md_input)


@dataclass
class Run(BaseMD):
    """This class handles all the metadata attributes at the run level."""
    acq_by_author: Optional[str] = field(default=None,
                                         metadata={'xml_id': ['comments', 'mt.run:a.acquired_by.author'],
                                                   'req': False, 'gui': True})
    acq_by_comments: Optional[str] = field(default=None,
                                           metadata={'xml_id': ['comments', 'mt.run:a.acquired_by.comments'],
                                                     'req': False, 'gui': True})
    comps_rec: Optional[str] = field(default=None,
                                     metadata={'req': True, 'gui': True})
    comments: Optional[str] = field(default=None,
                                    metadata={'xml_id': ['comments', 'mt.run:a.comments'],
                                              'req': False, 'gui': True})
    data_type: str = field(default='LPMT',
                           metadata={'xml_id': ['type'],
                                     'req': True, 'gui': False})
    datalogger_type: str = field(default='type: long-period 32-bit',
                                 metadata={'xml_id': ['description'],
                                           'req': False, 'gui': False})
    datalogger_manufacturer: str = field(default='LEMI LLC.',
                                         metadata={'xml_id': ['manufacturer'],
                                                   'req': True, 'gui': False})
    datalogger_model: str = field(default='LEMI-424',
                                  metadata={'xml_id': ['model'],
                                            'req': True, 'gui': True})
    datalogger_sn: Optional[str] = field(default=None,
                                         metadata={'xml_id': ['serial_number'],
                                                   'req': True, 'gui': True})
    md_by_author: Optional[str] = field(default=None,
                                        metadata={'xml_id': ['comments', 'mt.run:a.metadata_by.author'],
                                                  'req': False, 'gui': True})
    md_by_comments: Optional[str] = field(default=None,
                                          metadata={'xml_id': ['comments', 'mt.run:a.metadata_by.comments'],
                                                    'req': False, 'gui': True})
    resource_id: Optional[str] = field(default=None,
                                       metadata={'xml_id': ['resource_id'],
                                                 'req': True, 'gui': False})
    end: Optional[UTCDateTime] = field(default=None,
                                       metadata={'xml_id': ['removal_date'],
                                                 'req': True, 'gui': False})
    start: Optional[UTCDateTime] = field(default=None,
                                         metadata={'xml_id': ['installation_date'],
                                                   'req': True, 'gui': False})

    @property
    def run_id(self) -> str:
        return self.resource_id.split(':')[1]  # type: ignore

    def validate_comps_rec(self, md_input: str) -> bool:
        """
        This method is only useful when working with the initial version of the
        install sheet. This version was released before the lemi2seed MVP. The
        new and final version of the install sheet has a drop down option for
        the "components recorded" metadata field preventing any formatting issue
        or unexpected value for that field.
        The following channels: Te, Tf, Ui, Sn, Fq, Ce (Sn: Satellite number,
        Fq: GPS Fix quality, Ce: Clock error) are recorded by default.
        """
        try:

            comps = str2list(md_input, sep='-') if '-' in md_input else str2list(md_input)
        except (AttributeError, TypeError):
            logger.error("The 'components recorded' metadata field is empty "
                         "for run '{}'. Example of valid field: {}"
                         .format(self.run_id, VALID_COMPS[0]))
            return False
        else:
            if ' - '.join(comps) not in VALID_COMPS:
                logger.error("Invalid set of recorded components for run '{}'. "
                             "Valid options are: {}."
                             .format(self.run_id, ' or '.join(VALID_COMPS)))
                return False
            return True

    def validate_datalogger_sn(self, md_input: str, data_input: Optional[str]) -> bool:
        equipment = "data logger for run '{}'".format(self.run_id)
        return check_sn(equipment, md_input, data_input)


@dataclass
class BaseChannel(BaseSta):
    """
    This class handles the metadata attributes shared at the electric, magnetic
    and auxiliary levels.
    """
    cha_name: Optional[str] = field(default=None,
                                    metadata={'req': True, 'gui': False})
    clock_drift: float = field(default=CLOCKDRIFT,
                               metadata={'xml_id': ['clock_drift_in_seconds_per_sample'],
                                         'req': False, 'gui': False})
    comp: Optional[str] = field(default=None,
                                metadata={'xml_id': ['alternate_code'],
                                          'req': True, 'gui': False})
    data_type: str = field(default='GEOPHYSICAL',
                           metadata={'xml_id': ['types'],
                                     'req': True, 'gui': False})
    depth: int = field(default=0,
                       metadata={'req': True, 'gui': False})
    inst_manufacturer: Optional[str] = field(default=None,
                                             metadata={'req': True, 'gui': True})
    inst_model: Optional[str] = field(default=None,
                                      metadata={'req': True, 'gui': True})
    inst_specs: Optional[str] = field(default=None,
                                      metadata={'req': True, 'gui': True})
    inst_type: Optional[str] = field(default=None,
                                     metadata={'req': False, 'gui': True})
    loc_code: str = field(default='',
                          metadata={'req': True, 'gui': False})
    meas_azimuth: Optional[float] = field(default=None,
                                          metadata={'xml_id': ['azimuth'],
                                                    'req': True, 'gui': True})
    meas_tilt: Optional[float] = field(default=None,
                                       metadata={'xml_id': ['dip'],
                                                 'req': True, 'gui': True})
    run_id: Optional[str] = field(default=None,
                                  metadata={'req': True, 'gui': False})
    sample_rate: float = field(default=SAMPLING_RATE,
                               metadata={'xml_id': ['sample_rate'],
                                         'req': True, 'gui': True})

    def validate_meas_azimuth(self, md_input: float) -> bool:
        if isinstance(self, Elec):
            geo = "azimuth angle of electrode pair {0} (run '{1}')".format(self.pair_num, self.run_id)
        if isinstance(self, Mag):
            geo = "azimuth angle of fluxgate (run '{}')".format(self.run_id)
        param = (geo, 0, 360, MINIMUM_GEO, MAXIMUM_GEO, None, '°')
        return BaseSta.validate_geos(param, md_input)

    def validate_meas_tilt(self, md_input: float) -> bool:
        if isinstance(self, Elec):
            geo = "tilt angle of electrode pair {0} (run '{1}')".format(self.pair_num, self.run_id)
        if isinstance(self, Mag):
            geo = "tilt angle of fluxgate (run '{}')".format(self.run_id)
        param = (geo, -90, 90, MINIMUM_GEO, MAXIMUM_GEO, None, '°')
        return BaseSta.validate_geos(param, md_input)


@dataclass
class Elec(BaseChannel):
    """This class handles all the metadata attributes at the electric level."""
    cha_port: Optional[str] = field(default=None,
                                    metadata={'xml_id': ['comments', 'mt.electric.channel_number'],
                                              'req': True, 'gui': True})
    contact_resistance_end: Optional[float] = field(default=None,
                                                    metadata={'req': False, 'gui': True})
    contact_resistance_start: Optional[float] = field(default=None,
                                                      metadata={'req': False, 'gui': True})
    dc_end: Optional[float] = field(default=None,
                                    metadata={'req': False, 'gui': True})
    dc_start: Optional[float] = field(default=None,
                                      metadata={'req': False, 'gui': True})
    dipole_len: Optional[float] = field(default=None,
                                        metadata={'req': True, 'gui': True})
    neg_elec_dir: Optional[str] = field(default=None,
                                        metadata={'req': True, 'gui': True})
    neg_elec_sn: Optional[str] = field(default=None,
                                       metadata={'req': True, 'gui': True})
    pair_num: Optional[int] = field(default=None,
                                    metadata={'req': True, 'gui': False})
    pos_elec_dir: Optional[str] = field(default=None,
                                        metadata={'req': True, 'gui': True})
    pos_elec_sn: Optional[str] = field(default=None,
                                       metadata={'req': True, 'gui': True})

    def set_elec_comp_cha(self) -> None:
        """
        Set component and channel name for a given electric channel based on
        the direction of its electrode pair.
        """
        try:
            dir = "-".join([self.pos_elec_dir, self.neg_elec_dir])  # type: ignore
        except TypeError:
            logger.error("The direction of the positive and negative "
                         "electrodes should be either: North, South, East or "
                         "West. Please, provide electrode direction(s) for "
                         "run '{0}' and electrode pair {1}!"
                         .format(self.run_id, self.pair_num))
            self.md_invalid.update({'pos_elec_dir', 'neg_elec_dir'})
        else:
            comp = E_COMP_NAMING_CONV.get(dir)
            if comp is None:
                if self.pos_elec_dir == self.neg_elec_dir:
                    logger.error("The direction of the positive and negative "
                                 "electrodes in a given pair cannot be the "
                                 "same (check run '{0}' and electrode pair {1})!"
                                 .format(self.run_id, self.pair_num))
                logger.warning("By convention, one electrode pair is installed "
                               "in a north-south direction and the other pair "
                               "in a east-west direction (check run '{0}' and "
                               "electrode pair {1})!"
                               .format(self.run_id, self.pair_num))
                self.md_invalid.update({'pos_elec_dir', 'neg_elec_dir'})
            else:
                self.comp = comp
                self.cha_name = CHA_NAMING_CONV[comp]
                self.md_invalid.discard('pos_elec_dir')
                self.md_invalid.discard('neg_elec_dir')

    def set_elec_info(self) -> None:
        """
        Set electrode manufacturer, model and type based on user provided
        electrode specs.
        """
        if self.inst_specs == ELEC_DEFAULT:
            self.inst_manufacturer = "Borin"
            self.inst_model = "STELTH 4"
            self.inst_type = "Silver-Silver Chloride"
        elif self.inst_specs is not None:
            specs = [x.split(':')[-1].strip() for x in self.inst_specs.split('-')]
            self.inst_manufacturer, self.inst_model, self.inst_type = specs

    def validate_inst_specs(self, md_input: str) -> bool:
        equipment = 'elec'
        cha_info = " for run '{0}' and electrode pair '{1}'".format(self.run_id, self.pair_num)
        return check_inst_specs(md_input, equipment, cha_info)

    def validate_e_prop(self, param: tuple, md_input: float) -> bool:
        e_prop, min_range, max_range, units = param
        try:
            md_input = float(md_input)
        except (TypeError, ValueError):
            logger.error("The {0} for run '{1}' and electrode pair {2} should "
                         "be a float.".format(e_prop, self.run_id, self.pair_num))
            return False
        else:
            if md_input < min_range:
                logger.error("The {0} for run '{1}' and for electrode pair {2} "
                             "should be positive.".format(e_prop, self.run_id, self.pair_num))
                return False
            if md_input > max_range:
                msg = ''
                if 'resistance' in e_prop or 'voltage' in e_prop:
                    msg = ("A {0} > {1}{2} is indicative of poor contact."
                           .format(e_prop, max_range, units))
                logger.warning("The {0} for run '{1}' and for electrode pair "
                               "{2} should be less than {3}{4}. {5}"
                               .format(e_prop, self.run_id, self.pair_num,
                                       max_range, units, msg))
            return True

    def validate_contact_resistance_end(self, md_input: float) -> bool:
        if md_input is not None:
            param = ("contact resistance (end)", 0, 3000, 'Ω')
            return self.validate_e_prop(param, md_input)
        return True

    def validate_contact_resistance_start(self, md_input: float) -> bool:
        if md_input is not None:
            param = ("contact resistance (start)", 0, 3000, 'Ω')
            return self.validate_e_prop(param, md_input)
        return True

    def validate_dc_end(self, md_input: float) -> bool:
        if md_input is not None:
            param = ("DC voltage (end)", 0, 0.1, 'V')
            return self.validate_e_prop(param, md_input)
        return True

    def validate_dc_start(self, md_input: float) -> bool:
        if md_input is not None:
            param = ("DC voltage (start)", 0, 0.1, 'V')
            return self.validate_e_prop(param, md_input)
        return True

    def validate_dipole_len(self, md_input: float, data_input: Dict) -> bool:
        param = ("dipole length", 0, 99999, 'm')
        if not self.validate_e_prop(param, md_input):
            return False
        inf_dipole_len = data_input.get(self.cha_port)
        if inf_dipole_len is not None:
            valid = (float(md_input) == inf_dipole_len)
            if not valid:
                logger.error("The dipole length for run '{0}' and electrode "
                             "pair {1} does not match the dipole length value "
                             "that was configured using the LEMI-424 logger in "
                             "the field ({2} != {3})! Update value using the GUI."
                             .format(self.run_id, self.pair_num, md_input, inf_dipole_len))
            return valid
        else:
            return True

    def validate_neg_elec_sn(self, md_input: str) -> bool:
        equipment = ("negative electrode (run '{0}' and electrode pair {1})"
                     .format(self.run_id, self.pair_num))
        return check_sn(equipment, md_input)

    def validate_pos_elec_sn(self, md_input: str) -> bool:
        equipment = ("positive electrode (run '{0}' and electrode pair {1})"
                     .format(self.run_id, self.pair_num))
        return check_sn(equipment, md_input)


@dataclass
class Mag(BaseChannel):
    """This class handles all the metadata attributes at the magnetic level."""
    fluxgate_sn: Optional[str] = field(default=None,
                                       metadata={'req': True, 'gui': True})

    def set_mag_info(self) -> None:
        if self.inst_specs == FLUXGATE_DEFAULT:
            self.inst_manufacturer = "LEMI LLC."
            self.inst_model = "LEMI-039"
            self.inst_type = "3-component analog magnetometer"
        elif self.inst_specs is not None:
            specs = [x.split(':')[-1].strip() for x in self.inst_specs.split('-')]
            self.inst_manufacturer, self.inst_model, self.inst_type = specs

    def validate_inst_specs(self, md_input: str) -> bool:
        equipment = 'fluxgate'
        cha_info = " for run '{0}'".format(self.run_id)
        return check_inst_specs(md_input, equipment, cha_info)

    def validate_fluxgate_sn(self, md_input: str, data_input: Optional[str]) -> bool:
        equipment = "fluxgate (run '{}')".format(self.run_id)
        return check_sn(equipment, md_input, data_input)


@dataclass
class Aux(BaseChannel):
    """This class handles all the metadata attributes at the auxiliary level."""
    sn: Optional[str] = field(default=None,
                              metadata={'req': True, 'gui': False})

    def set_aux_info(self) -> None:
        self.inst_specs = ""
        self.inst_manufacturer = "LEMI LLC."
        self.inst_model = "LEMI-424"
        self.inst_type = "long-period 32-bit"

    def set_data_type(self):
        self.data_type = 'HEALTH'

    def set_meas_azimuth(self):
        self.meas_azimuth = 0.0

    def set_meas_tilt(self):
        self.meas_tilt = 0.0
