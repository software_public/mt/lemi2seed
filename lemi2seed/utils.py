# -*- coding: utf-8 -*-
"""
Various "utils" functions required for data and metadata formatting and
validation.

Maeva Pourpoint - IRIS/PASSCAL
"""

from __future__ import annotations

import re

from obspy import UTCDateTime
from typing import Any, Dict, List, Optional, Tuple

from lemi2seed.logging import setup_logger

# Set up logging
logger = setup_logger(__name__)

ELEC_DEFAULT = 'Borin STELTH 4 - Silver-Silver Chloride'
FLUXGATE_DEFAULT = 'LEMI-039'
LOC = ['00', '01', '02', '03']
# Maximum number of electrode pairs
NUM_E_CHA_MAX = 4


def check_email_format(email: str) -> bool:
    """
    Basic email check. Check for proper formatting.
    Will not catch typos or fake email addresses.
    """
    email_pattern = r"[\w\.\-_]+@[\w\.\-_]+"
    try:
        valid = re.match(email_pattern, email)
    except TypeError:
        logger.error("The provided email '{}' should be a string.".format(email))
        return False
    else:
        if valid is None:
            logger.error("Invalid email. The provided email '{}' does not "
                         "meet minimum formatting requirements: "
                         "account@domain.".format(email))
        return bool(valid)


def check_inst_specs(specs: str, equipment: str, cha_info: str = "") -> bool:
    """
    Check instrument specifications provided by the user.
    Both manufacturer and model info are required inputs. Type is optional.
    """
    if specs in [ELEC_DEFAULT, FLUXGATE_DEFAULT]:
        return True
    elif specs is not None and re.match(r"^Manufacturer: \w* - Model: \w* - Type: \w*$", specs):
        manufacturer, model, _ = [x.split(':')[-1].strip() for x in specs.split('-')]
        if manufacturer == '':
            logger.error("Please provide {0} manufacturer{1}. Required "
                         "metadata field.".format(equipment, cha_info))
            return False
        if model == '':
            logger.error("Please provide {0} model{1}. Required metadata "
                         "field.".format(equipment, cha_info))
            return False
        return True
    else:
        logger.error("Please provide {0} specs for{1}. Required metadata "
                     "field.".format(equipment, cha_info))
        return False


def compare_sn(equipment: str, sn: str, data_input: Optional[str] = None) -> bool:
    """
    Compare user provided serial number for the data logger or fluxgate against
    the serial number recorded in the .INF file.
    For the data logger and fluxgate, the serial number should also be 3 digits
    long.
    """
    if data_input is not None:
        valid = (str(sn) == data_input)
        if not valid:
            logger.error("The serial number of the {} differs from the one "
                         "automatically recorded by the logger in the field "
                         "({} != {}). Please correct!".format(equipment, sn, data_input))
    else:
        valid = re.match(r"^\d{3}$", str(sn))  # type: ignore
        valid = False if valid is None else True
        if not valid:
            logger.error("The serial number of the {} should be 3 digits "
                         "long.".format(equipment))
    return valid


def check_sn(equipment: str, sn: str, data_input: Optional[str] = None) -> bool:
    """
    Check user provided serial number for their data logger, fluxgate or
    electrodes.
    """
    if sn is not None:
        valid = True
        if 'data logger' in equipment or 'fluxgate' in equipment:
            valid = compare_sn(equipment, sn, data_input)
        return valid
    else:
        msg = "Please provide a serial number for the {}!".format(equipment)
        logger.error(msg)
        return False


def convert_coord(coord: str, hemisphere: str) -> Optional[float]:
    """Convert coordinates outputted by LEMI to decimal degrees."""
    if hemisphere not in ['N', 'S', 'E', 'W']:
        logger.error("Unexpected hemisphere - {} - listed in data file!"
                     .format(hemisphere))
        return None
    try:
        coord = float(coord) / 100  # type: ignore
    except ValueError:
        logger.error("Failed to convert geographic coordinate - {} - to "
                     "decimal degrees!".format(coord))
        return None
    return -coord if hemisphere in ["S", "W"] else coord  # type: ignore


def convert_time(time_stamp: str) -> Optional[UTCDateTime]:
    """Convert time stamp recorded by LEMI to UTC."""
    msg = ("Failed to convert time stamp - {} - to UTC!".format(time_stamp))
    # Check that time_stamp is properly formatted - Otherwise this may indicate
    # that the input file was corrupted
    if re.match(r"^\d{4} \d{2} \d{2} \d{2} \d{2} \d{2}$", time_stamp):
        try:
            time_stamp = UTCDateTime(time_stamp)
            return time_stamp
        except ValueError:
            logger.error(msg)
            return None
    logger.error(msg)
    return None


def eval_loc_code(e_infos: Dict) -> bool:
    """
    Get location code boolean - Needed to define whether location code
    should be '' (i.e. undefined)
    """
    for _, e_info in e_infos.items():
        if list(e_info.values()).count('Ex') > 1 or list(e_info.values()).count('Ey') > 1:
            return True
    return False


def get_e_ids(num_runs: int) -> List[Tuple[str, int]]:
    """Get permutation of run ids and channel numbers."""
    run_list = get_run_list(num_runs)
    return [(i, j) for i in run_list for j in range(1, NUM_E_CHA_MAX + 1)]


def get_e_loc(e_info: Dict) -> Dict:
    """
    Get location code for electrode pairs.
    Only useful when more than two electrode pairs are deployed at a station.
    """
    e_loc = {}
    comps = {x for x in e_info.values() if x}
    for comp in comps:
        cha_port = [key for key, val in e_info.items() if val == comp]
        e_loc.update({key: LOC[ind] for ind, key in enumerate(cha_port)})
    return e_loc


def get_run_list(num_runs: int) -> List[str]:
    """Get list of run ids."""
    return [chr(x) for x in range(ord('a'), ord('a') + num_runs)]


def get_run_num(run_id: str) -> int:
    """Get the integer representation of the string run_id."""
    integer_representation_unicode_character_a = 97
    run_num = ord(run_id) - integer_representation_unicode_character_a + 1
    return run_num


def is_empty(val: Any) -> bool:
    """Check that specific data structure is empty."""
    if val is None:
        return True
    if (isinstance(val, list) or isinstance(val, dict)) and not val:
        return True
    return False


def is_valid_uri(uri: str) -> bool:
    """From obpsy.core.inventory.util private function _is_valid_uri"""
    if ':' not in uri:
        return False
    scheme, path = uri.split(':', 1)
    if any(not x.strip() for x in (scheme, path)):
        return False
    return True


def str2list(str_input: str, sep: Optional[str] = ',') -> List[str]:
    """Convert comma separated string input to a list of strings."""
    return [x.strip() for x in str_input.split(sep) if x != '']
