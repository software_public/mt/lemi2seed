#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst', 'rt') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst', 'rt') as history_file:
    history = history_file.read()


setup(
    author="IRIS PASSCAL",
    author_email='software-support@passcal.nmt.edu',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved ::  GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11'
    ],
    description="Convert LEMI data and metadata into day-long mseed and "
                "StationXML files.",
    entry_points={
        'console_scripts': [
            'lemi2seed=lemi2seed.lemi2seed:main',
        ],
    },
    install_requires=['configobj',
                      'numpy',
                      'obspy',
                      'openpyxl',
                      'PySide6'],
    setup_requires=[],
    extras_require={
        'dev': [
            'flake8',
            'tox',
            'mypy',
            'coverage',
            'genbadge',
        ]
    },
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='lemi2seed',
    name='lemi2seed',
    packages=find_packages(include=['lemi2seed']),
    url='https://git.passcal.nmt.edu/software_public/mt/lemi2seed',
    version='2023.2.0.0',
    zip_safe=False,
)
