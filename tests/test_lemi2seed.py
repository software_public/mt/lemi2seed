# -*- coding: utf-8 -*-

"""Tests for `lemi2seed` module."""

import unittest

from pathlib import Path
from unittest.mock import patch

from lemi2seed.lemi2seed import (check_output_dir, get_args, get_net_inputs,
                                 get_sta_inputs, get_num_inputs, get_dir_inputs,
                                 get_cha_port_inputs, get_elec_inputs,
                                 get_qc_inputs, reformat_user_inputs, main)
from lemi2seed.logging import setup_logger

TEST_DIR = Path(__file__).resolve().parent.joinpath('test_data')
SCR_DIR = "lemi2seed.lemi2seed"

# Set up logging
logger = setup_logger(SCR_DIR)


class TestArgsFunctions(unittest.TestCase):
    """Test suite for check_output_dir and get_args functions."""

    def setUp(self):
        """Set up test fixtures."""
        self.path2data = TEST_DIR.joinpath("EM", "TEST1")
        self.path2metadata = TEST_DIR.joinpath("METADATA")
        self.path2savedmd = TEST_DIR.joinpath("METADATA", "gui_metadata_populated.pkl")
        self.output_mseed = Path(__file__).resolve().parent.joinpath('MSEED')

    def test_check_output_dir_creation(self):
        """Test basic functionality of check_output_dir."""
        with patch("lemi2seed.lemi2seed.Path.mkdir") as mock_makedirs:
            check_output_dir(self.output_mseed, 'day-long mseed')
        self.assertEqual(mock_makedirs.call_count, 1)
        self.assertTrue(mock_makedirs.called_with(self.output_mseed))

    def test_check_output_dir_oserror(self):
        """Test basic functionality of check_output_dir."""
        with patch("lemi2seed.lemi2seed.Path.mkdir", side_effect=OSError):
            with self.assertRaises(SystemExit) as cmd:
                check_output_dir(self.output_mseed, 'day-long mseed')
        self.assertEqual(cmd.exception.code, 1)

    def test_get_args_no_inputs(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed"]):
            with self.assertRaises(SystemExit) as cmd:
                get_args()
            self.assertEqual(cmd.exception.code, 1)

    def test_get_args_path2data_does_not_exist(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data) + '_']):
            with self.assertRaises(SystemExit) as cmd:
                get_args()
        self.assertEqual(cmd.exception.code, 1)

    def test_get_args_qc_mode_and_gui_only_mode(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data), "-qc", "-g"]):
            with self.assertLogs(logger, level='ERROR') as cmd1:
                with self.assertRaises(SystemExit) as cmd2:
                    get_args()
        msg = ("lemi2seed cannot run under both the 'quality control' and "
               "'gui only' mode at the same time! Try --help option or choose "
               "one or the other mode.")
        self.assertEqual(cmd1.output, [":".join(['ERROR', SCR_DIR, msg])])
        self.assertEqual(cmd2.exception.code, 1)

    def test_get_args_path2data_only(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data)]):
            with self.assertLogs(logger, level='ERROR') as cmd1:
                with self.assertRaises(SystemExit) as cmd2:
                    get_args()
        msg = ("No metadata path provided. Required input when running in "
               "'field sheet and gui' mode! Try --help option.")
        self.assertEqual(cmd1.output, [":".join(['ERROR', SCR_DIR, msg])])
        self.assertEqual(cmd2.exception.code, 1)

    def test_get_args_path2metadata_qc_only(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata), "-qc"]):
            with self.assertLogs(logger, level='INFO') as cmd:
                with patch("lemi2seed.lemi2seed.Path.mkdir"):
                    _, path2metadata, *other = get_args()
        msg = ("Handling of the field sheets is bypassed when running in "
               "'quality control' mode.")
        self.assertEqual(cmd.output, [":".join(['INFO', SCR_DIR, msg])])
        self.assertIsNone(path2metadata)

    def test_get_args_path2metadata_gui_only(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata), "-g"]):
            with self.assertLogs(logger, level='INFO') as cmd:
                with patch("lemi2seed.lemi2seed.Path.mkdir"):
                    _, path2metadata, *other = get_args()
        msg = ("All metadata should be entered using the GUI in 'gui only' "
               "mode. Bypassing the use of field sheets.")
        self.assertEqual(cmd.output, [":".join(['INFO', SCR_DIR, msg])])
        self.assertIsNone(path2metadata)

    def test_get_args_path2metadata_field_sheet_and_gui(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata)]):
            with patch("lemi2seed.lemi2seed.Path.mkdir"):
                path2data, path2metadata, qc_only, gui_only, *other = get_args()
        self.assertEqual(path2data, self.path2data)
        self.assertEqual(path2metadata, self.path2metadata)
        self.assertFalse(qc_only)
        self.assertFalse(gui_only)

    def test_get_args_path2metadata_do_not_exist_gui_only(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata) + "_", "-g"]):
            with self.assertLogs(logger, level='INFO') as cmd:
                with patch("lemi2seed.lemi2seed.Path.mkdir"):
                    _, path2metadata, *other = get_args()
        msg1 = "The path to the LEMI field spreadsheets does not exist!"
        msg2 = ("In 'quality control' or 'gui only' mode, field sheets are not "
                "parsed.")
        self.assertEqual(cmd.output[0], ":".join(['ERROR', SCR_DIR, msg1]))
        self.assertEqual(cmd.output[1], ":".join(['INFO', SCR_DIR, msg2]))
        self.assertIsNone(path2metadata)

    def test_get_args_path2metadata_do_not_exist_qc_only(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata) + "_", "-qc"]):
            with self.assertLogs(logger, level='INFO') as cmd:
                with patch("lemi2seed.lemi2seed.Path.mkdir"):
                    _, path2metadata, *other = get_args()
        msg1 = "The path to the LEMI field spreadsheets does not exist!"
        msg2 = ("In 'quality control' or 'gui only' mode, field sheets are not "
                "parsed.")
        self.assertEqual(cmd.output[0], ":".join(['ERROR', SCR_DIR, msg1]))
        self.assertEqual(cmd.output[1], ":".join(['INFO', SCR_DIR, msg2]))
        self.assertIsNone(path2metadata)

    def test_get_args_path2metadata_do_not_exist(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata) + "_"]):
            with self.assertLogs(logger, level='ERROR') as cmd1:
                with self.assertRaises(SystemExit) as cmd2:
                    get_args()
        msg1 = "The path to the LEMI field spreadsheets does not exist!"
        msg2 = ("A correct metadata path is a required input when running in "
                "'field sheet and gui' mode!")
        self.assertEqual(cmd1.output[0], ":".join(['ERROR', SCR_DIR, msg1]))
        self.assertEqual(cmd1.output[1], ":".join(['ERROR', SCR_DIR, msg2]))
        self.assertEqual(cmd2.exception.code, 1)

    def test_get_args_path2savedmd_qc_only(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-s" + str(self.path2savedmd), "-qc"]):
            with self.assertLogs(logger, level='INFO') as cmd:
                _, _, path2savedmd, *other = get_args()
        msg = ("Handling of previously saved metadata is bypassed when running "
               "in 'quality control' mode.")
        self.assertEqual(cmd.output[0], ":".join(['INFO', SCR_DIR, msg]))
        self.assertIsNone(path2savedmd)

    def test_get_args_path2savedmd_do_not_exist(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-s" + str(self.path2savedmd) + "_", "-g"]):
            with self.assertLogs(logger, level='ERROR') as cmd:
                _, _, path2savedmd, *other = get_args()
        msg = ("The provided path to a previously saved metadata file (*.pkl) "
               "does not exist! Bypassing loading of metadata fields "
               "before launching the GUI.")
        self.assertEqual(cmd.output[0], ":".join(['ERROR', SCR_DIR, msg]))
        self.assertIsNone(path2savedmd)

    def test_get_args_path2savedmd_gui_only(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-s" + str(self.path2savedmd), "-g"]):
            _, _, path2savedmd, *other = get_args()
        self.assertEqual(path2savedmd, self.path2savedmd)

    def test_get_args_path2savedmd_field_sheet_and_gui(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata),
                                "-s" + str(self.path2savedmd)]):
            with self.assertLogs(logger, level='INFO') as cmd:
                _, _, path2savedmd, *other = get_args()
        msg = ("The loading of a previously saved metadata file (*.pkl) is "
               "bypassed when running in 'field sheet and gui' mode. Try "
               "--help option for more information.")
        self.assertEqual(cmd.output[0], ":".join(['INFO', SCR_DIR, msg]))
        self.assertIsNone(path2savedmd)

    def test_get_args_output_xml_none(self):
        """Test basic functionality of get_args."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data), "-qc"]):
            *other, output_xml = get_args()
        self.assertIsNone(output_xml)


class TestGetQcInputs(unittest.TestCase):
    """Test suite for get_qc_inputs."""

    @patch('builtins.input', side_effect=['EM'])
    def test_get_net_inputs(self, mock_input):
        """Test basic functionality of get_net_inputs."""
        user_inputs = get_net_inputs()
        self.assertEqual(user_inputs, 'EM')

    @patch('builtins.input', side_effect=['KELLY'])
    def test_get_sta_inputs(self, mock_input):
        """Test basic functionality of get_sta_inputs."""
        user_inputs = get_sta_inputs()
        self.assertEqual(user_inputs, 'KELLY')

    @patch('builtins.input', side_effect=[0])
    def test_get_num_inputs(self, mock_input):
        """Test basic functionality of get_num_inputs."""
        user_inputs = get_num_inputs()
        self.assertEqual(user_inputs, 0)

    @patch('builtins.input', side_effect=['E1'])
    def test_get_cha_port_inputs(self, mock_input):
        """Test basic functionality of get_cha_port_inputs."""
        user_inputs = get_cha_port_inputs(1)
        self.assertEqual(user_inputs, 'E1')

    @patch('builtins.input', side_effect=[1])
    def test_get_dir_inputs(self, mock_input):
        """Test basic functionality of get_dir_inputs."""
        user_inputs = get_dir_inputs(2)
        self.assertEqual(user_inputs, 1)

    @patch('builtins.input', side_effect=['EM', 'KELLY', 0])
    def test_get_elec_inputs_zero_electrode_pair(self, mock_input):
        """Test basic functionality of get_elec_inputs."""
        user_inputs = {}
        user_inputs['net'] = get_net_inputs()
        user_inputs['sta'] = get_sta_inputs()
        user_inputs['e_chas'] = []
        user_inputs['e_info'] = {}
        user_inputs = get_elec_inputs(user_inputs)
        self.assertDictEqual(user_inputs,
                             {'net': 'EM', 'sta': 'KELLY',
                              'e_chas': [], 'e_info': {}})

    @patch('lemi2seed.lemi2seed.VALID_CHA_PORTS', ['E1', 'E2', 'E3', 'E4'])
    @patch('builtins.input', side_effect=['EM', 'KELLY', 2, 'E1', 1, 'E2', 2])
    def test_get_elec_inputs_two_electrode_pairs(self, mock_input):
        """Test basic functionality of get_elec_inputs."""
        user_inputs = {}
        user_inputs['net'] = get_net_inputs()
        user_inputs['sta'] = get_sta_inputs()
        user_inputs['e_chas'] = []
        user_inputs['e_info'] = {}
        user_inputs = get_elec_inputs(user_inputs)
        self.assertDictEqual(user_inputs,
                             {'net': 'EM', 'sta': 'KELLY',
                              'e_chas': ['E1', 'E2'],
                              'e_info': {'E1': 'Ex', 'E2': 'Ey'}})

    @patch('lemi2seed.lemi2seed.VALID_CHA_PORTS', ['E1', 'E2', 'E3', 'E4'])
    @patch('builtins.input', side_effect=['EM', 'KELLY', 4, 'E1', 2, 'E2', 1, 'E3', 1, 'E4', 2])
    def test_get_qc_inputs(self, mock_input):
        """Test basic functionality of get_qc_inputs."""
        user_inputs = get_qc_inputs()
        self.assertDictEqual(user_inputs,
                             {'net': 'EM', 'sta': 'KELLY',
                              'e_chas': ['E1', 'E2', 'E3', 'E4'],
                              'e_info': {'E1': 'Ey', 'E2': 'Ex', 'E3': 'Ex', 'E4': 'Ey'}})


class TestMain(unittest.TestCase):
    """Test suites for main function"""

    def setUp(self):
        """Set up test fixtures."""
        self.path2data = TEST_DIR.joinpath("EM", "TEST1")
        self.path2metadata = TEST_DIR.joinpath("METADATA")

    def test_reformat_user_inputs(self):
        """Test basic functionality of reformat_user_inputs."""
        user_inputs = {'net': 'EM',
                       'sta': 'KELLY',
                       'e_chas': ['E1', 'E2'],
                       'e_info': {'E1': 'Ex', 'E2': 'Ey'}}
        msg = reformat_user_inputs(user_inputs)
        self.assertEqual(msg,
                         "  Network code: EM\n  Station name: KELLY\n"
                         "  Number of electrode pairs installed: 2\n"
                         "  Channel port number for electrode pair 1: E1\n"
                         "  Direction of electrode pair 1: North-South\n"
                         "  Channel port number for electrode pair 2: E2\n"
                         "  Direction of electrode pair 2: East-West")

    def test_main_qc_mode(self):
        """Test basic fonctionality of main."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data), "-qc"]):
            with patch("lemi2seed.lemi2seed.Path.mkdir"):
                with patch('lemi2seed.lemi2seed.VALID_CHA_PORTS', ['E1', 'E2', 'E3', 'E4']):
                    with patch("builtins.input", side_effect=['EM', 'KELLY', 2, 'E1', 1, 'E2', 2]):
                        with self.assertLogs(logger, level='INFO') as cmd:
                            with patch("lemi2seed.lemi2seed.write_daylong_mseed") as mock_write_mseed:
                                main()
        expected_msg = ["INFO:lemi2seed.lemi2seed:Running lemi2seed in "
                        "'quality control' mode.", "INFO:lemi2seed."
                        "lemi2seed:Writing day-long mseed files."]
        self.assertEqual(cmd.output[0], expected_msg[0])
        self.assertEqual(cmd.output[-1], expected_msg[-1])
        self.assertEqual(mock_write_mseed.call_count, 1)

    def test_main_gui_mode(self):
        """Test basic fonctionality of main."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data), "-g"]):
            with patch("lemi2seed.lemi2seed.Path.mkdir"):
                with self.assertLogs(logger, level='INFO') as cmd:
                    with patch("lemi2seed.lemi2seed.launch_gui") as mock_gui:
                        main()
        expected_msg = ["INFO:lemi2seed.lemi2seed:Running lemi2seed in "
                        "'gui only' mode.", "INFO:lemi2seed."
                        "lemi2seed:Launching the GUI."]
        self.assertEqual(cmd.output, expected_msg)
        self.assertEqual(mock_gui.call_count, 1)

    def test_main_field_sheet_and_gui_mode(self):
        """Test basic fonctionality of main."""
        with patch("sys.argv", ["lemi2seed", "-d" + str(self.path2data),
                                "-m" + str(self.path2metadata)]):
            with patch("lemi2seed.lemi2seed.Path.mkdir"):
                with self.assertLogs(logger, level='INFO') as cmd:
                    with patch("lemi2seed.lemi2seed.launch_gui") as mock_gui:
                        main()
        expected_msg = ["INFO:lemi2seed.lemi2seed:Running lemi2seed in "
                        "'field sheet and gui' mode.", "INFO:lemi2seed."
                        "lemi2seed:Pre-populating the GUI metadata fields "
                        "using the information provided in the field sheet(s).",
                        "INFO:lemi2seed.lemi2seed:Launching the GUI."]
        self.assertEqual(cmd.output, expected_msg)
        self.assertEqual(mock_gui.call_count, 1)
