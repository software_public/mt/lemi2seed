# -*- coding: utf-8 -*-

"""Tests for `lemi_response` module."""

import unittest

from obspy.core.inventory import Response
from pathlib import Path
from unittest.mock import patch

from lemi2seed.logging import setup_logger
from lemi2seed.lemi_response import LemiResponse, RESP_FILE

TEST_DIR = Path(__file__).resolve().parent.joinpath('test_data')
SCR_DIR = "lemi2seed.lemi_response"

# Set up logging
logger = setup_logger(SCR_DIR)


class TestLemiResponse(unittest.TestCase):
    """Test suite for lemi_response.Response class."""

    @patch('lemi2seed.lemi_response.RESP_FILE', RESP_FILE.parent.joinpath('test'))
    def test_check_resp_file(self):
        """Test basic functionality of check_resp."""
        with self.assertRaises(SystemExit) as cmd1:
            with self.assertLogs(logger, level='ERROR') as cmd2:
                LemiResponse.check_resp_file()
        msg = ("No response file 'test' found under the following directory: {}. "
               "Exiting ...".format(RESP_FILE.parent))
        self.assertEqual(cmd1.exception.code, 1)
        self.assertEqual(cmd2.output, [":".join(['ERROR', SCR_DIR, msg])])

    @patch('lemi2seed.lemi_response.RESP_FILE', TEST_DIR.joinpath('RESPONSE_FILE', 'LEMI-424_Response.zip'))
    def test_load_resp_corrupted(self):
        """Test basic functionality of load_resp."""
        with self.assertRaises(SystemExit) as cmd1:
            with self.assertLogs(logger, level='ERROR') as cmd2:
                LemiResponse.load_resp()
        msg = ("Failed to 'unpickled' response file LEMI-424_Response.zip. The "
               "file may have been corrupted. Exiting ...")
        self.assertEqual(cmd1.exception.code, 1)
        self.assertEqual(cmd2.output, [":".join(['ERROR', SCR_DIR, msg])])

    def test_load_resp_not_corrupted(self):
        """Test basic functionality of load_resp."""
        resps = LemiResponse.load_resp()
        self.assertIsInstance(resps, dict)
        self.assertGreaterEqual(len(resps), 91)
        keys = resps.keys()
        for sn in ['N131', 'N132', 'N133', 'N134', 'N136', 'N137', 'N138',
                   'N139', 'N140', 'N142', 'N144', 'N145']:
            for cha in ['Hx', 'Hy', 'Hz', 'E1', 'E2', 'E3', 'E4']:
                for day in ['2022.189']:
                    self.assertIn(f'{sn}_{cha}_{day}', keys)

    def test_create_coefficient_filter(self):
        """Test basic functionality if create_coefficient_filter."""
        lemi_resp = LemiResponse('132', 'Ui', 1.0)
        cf_filter = lemi_resp.create_coefficient_filter()
        expected = {'_cf_transfer_function_type': 'DIGITAL',
                    '_numerator': [1.0],
                    '_denominator': [],
                    'stage_sequence_number': 1,
                    'input_units': 'V',
                    'output_units': 'V',
                    'input_units_description': 'electric potential in volts',
                    'output_units_description': 'electric potential in volts',
                    'resource_id': None,
                    'resource_id2': None,
                    'stage_gain': 1.0,
                    'stage_gain_frequency': 0.0,
                    'name': None,
                    'description': 'coefficient multiplier filter',
                    'decimation_input_sample_rate': 1.0,
                    'decimation_factor': 1,
                    'decimation_offset': 0,
                    'decimation_delay': 0.0,
                    'decimation_correction': 0.0}
        self.assertDictEqual(vars(cf_filter), expected)

    def test_get_data_cha_resp_no_matching_key(self):
        """Test basic functionality of get_data_cha_resp."""
        resps = LemiResponse.load_resp()
        lemi_resp = LemiResponse('120', 'Hx', 1.0)
        with self.assertRaises(SystemExit) as cmd1:
            with self.assertLogs(logger, level='ERROR') as cmd2:
                lemi_resp.get_data_cha_resp(resps)
        msg = ("No instrument response found for given channel '{}' and serial "
               "number '{}'. Exiting ...".format(lemi_resp.cha_code, lemi_resp.sn))
        self.assertEqual(cmd1.exception.code, 1)
        self.assertEqual(cmd2.output, [":".join(['ERROR', SCR_DIR, msg])])

    def test_get_data_cha_resp_matching_key(self):
        """Test basic funtionality of get_data_cha_resp."""
        resps = LemiResponse.load_resp()
        lemi_resp = LemiResponse('132', 'Hx', 1.0)
        data_cha_resp = lemi_resp.get_data_cha_resp(resps)
        self.assertIsInstance(data_cha_resp, Response)

    def test_get_soh_cha_resp(self):
        """Test basic funtionality of get_soh_cha_resp."""
        lemi_resp = LemiResponse('132', 'Ui', 1.0)
        soh_cha_resp = lemi_resp.get_soh_cha_resp()
        self.assertIsInstance(soh_cha_resp, Response)
