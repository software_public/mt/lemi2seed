# -*- coding: utf-8 -*-

"""Tests for `lemi_conversion` module."""

import pickle
import unittest

from dataclasses import fields
from obspy import UTCDateTime
from obspy.core.inventory import (Channel, Equipment, Inventory,
                                  CoefficientsTypeResponseStage,
                                  PolesZerosResponseStage)
from pathlib import Path

from lemi2seed.lemi_data import LemiData
from lemi2seed.lemi_metadata import LemiMetadata, config, CHA_TYPES
from lemi2seed.metadata_conversion import (create_inventory, create_net,
                                           create_sta, create_logger,
                                           create_elec_sensor,
                                           create_mag_sensor,
                                           create_aux_sensor, init_cha,
                                           create_cha, update_cha,
                                           detect_changes_elec_cha,
                                           detect_changes_datalogger,
                                           detect_changes_mag_cha,
                                           detect_new_epoch, get_xml_field,
                                           get_comment, get_e_cha_updates,
                                           get_cha_resp)

OUTPUT_MSEED = Path(__file__).resolve().parent.joinpath('MSEED')
OUTPUT_LOG = Path(__file__).resolve().parent.joinpath('LOG')
OUTPUT_XML = Path(__file__).resolve().parent.joinpath('STATIONXML')
TEST_DIR = Path(__file__).resolve().parent.joinpath('test_data')


class TestGetXmlFieldGetComment(unittest.TestCase):
    """Test suite for get_xml_field and get_comment functions."""

    def setUp(self):
        """Set up test fixtures"""
        lemi_data = LemiData(TEST_DIR.joinpath("EM", "TEST1"), OUTPUT_MSEED, OUTPUT_LOG)
        lemi_data.prep_data()
        path2md = TEST_DIR.joinpath("METADATA")
        lemi_md = LemiMetadata(path2md, OUTPUT_XML, lemi_data)
        md_fields = lemi_md.from_field_sheets
        lemi_md.populate_props(md_fields)
        self.md = lemi_md
        self.net_fields = [x for x in fields(self.md.net)
                           if x.metadata.get('xml_id') is not None]

    def test_get_xml_field(self):
        """Test basic functionality of get_xml_field."""
        field_info, field_value = get_xml_field(self.md.net, self.net_fields[0])
        self.assertListEqual(field_info, ['end_date'])
        self.assertEqual(field_value, UTCDateTime('2021-03-02T00:00:00.000000Z'))
        field_info, field_value = get_xml_field(self.md.net, self.net_fields[4])
        self.assertListEqual(field_info, ['comments', 'mt.network.comments'])
        self.assertEqual(field_value, 'Network Test')

    def test_get_comment_no_run_id_considered(self):
        """Test basic functionality of get_comment."""
        field_info, field_value = get_xml_field(self.md.net, self.net_fields[4])
        comment = get_comment(field_info, field_value)
        self.assertEqual(comment.value, 'Network Test')
        self.assertEqual(comment.subject, 'mt.network.comments')

    def test_get_comment_run_id_considered(self):
        """Test basic functionality of get_comment."""
        field_info = ['comments', 'mt.run:a.acquired_by.author']
        field_value = 'TK'
        comment = get_comment(field_info, field_value, ind_run=1)
        self.assertEqual(comment.value, 'TK')
        self.assertEqual(comment.subject, 'mt.run:b.acquired_by.author')


class TestCreateInventoryObjects(unittest.TestCase):
    """
    Test suite for create_inventory, create_net, create_sta, create_logger,
    create_elec_sensor, create_mag_sensor, create_aux_sensor, init_cha,
    create_cha, update_cha functions.
    """

    def setUp(self):
        """Set up test fixtures"""
        lemi_data = LemiData(TEST_DIR.joinpath("EM", "TEST5"), OUTPUT_MSEED, OUTPUT_LOG)
        lemi_data.prep_data()
        path2md = TEST_DIR.joinpath("METADATA")
        lemi_md = LemiMetadata(path2md, OUTPUT_XML, lemi_data)
        md_fields = lemi_md.from_field_sheets
        lemi_md.populate_props(md_fields)
        self.md = lemi_md
        self.path2stationxml = TEST_DIR.joinpath("STATIONXML")

    def test_create_inventory(self):
        """Test basic functionality of create_inventory."""
        inv = create_inventory()
        self.assertIsInstance(inv, Inventory)
        self.assertEqual(inv.module, f"lemi2seed.{config['software']['version']}")
        self.assertEqual(inv.module_uri, 'www.passcal.nmt.edu')
        self.assertEqual(inv.source, 'PASSCAL')

    def test_create_net(self):
        """Test basic functionality of create_net."""
        net = self.md.net
        net.start = UTCDateTime(2020, 10, 1, 0, 0)
        net.project = 'PIS_MT'
        net.project_lead_author = 'Maeva Pourpoint'
        net.project_lead_organization = 'PASSCAL'
        net.acquired_by_author = 'Maeva Pourpoint'
        net.country = 'USA'
        net.geo_name = 'Southwest'
        net.name = 'MT experiment in Kelly ranch'
        network = create_net(net)
        with open(self.path2stationxml.joinpath('network.pkl'), 'rb') as fin:
            expected_network = pickle.load(fin)
        self.assertEqual(network, expected_network)

    def test_create_sta(self):
        """Test basic functionality of create_sta."""
        sta = self.md.sta
        sta.start = UTCDateTime(2020, 9, 30, 0, 0)
        sta.end = UTCDateTime(2020, 10, 2, 0, 0)
        sta.lon = -107.13
        sta.elev = 2201
        sta.software_version = '2022.332'
        sta.submitter_author = 'David Goldak'
        sta.submitter_organization = 'PASSCAL'
        runs = self.md.run
        runs[0].md_by_author = 'Tom'
        runs[0].md_by_comments = 'Test - Run a'
        runs[1].md_by_author = 'Jerry'
        runs[1].md_by_comments = 'Test - Run b'
        station = create_sta(sta, runs)
        with open(self.path2stationxml.joinpath('station.pkl'), 'rb') as fin:
            expected_station = pickle.load(fin)
        self.assertEqual(station, expected_station)

    def test_create_logger(self):
        """Test basic functionality of create_logger."""
        loggers = create_logger(self.md.run)
        with open(self.path2stationxml.joinpath('equipment.pkl'), 'rb') as fin:
            expected_loggers = pickle.load(fin)
        self.assertListEqual(loggers, expected_loggers)

    def test_create_elec_sensor(self):
        """Test basic functionality of create_elec_sensor."""
        sensor = create_elec_sensor(self.md.elec[0])
        self.assertIsInstance(sensor, Equipment)
        self.assertEqual(sensor.type, 'dipole')
        self.assertEqual(sensor.manufacturer, 'Borin')
        self.assertIsNone(sensor.vendor)
        self.assertEqual(sensor.model, 'STELTH 4 - Silver-Silver Chloride')
        self.assertEqual(sensor.serial_number, 'positive: 1223, negative: 2345')

    def test_create_mag_sensor(self):
        """Test basic functionality of create_mag_sensor."""
        sensor = create_mag_sensor(self.md.mag[0])
        self.assertIsInstance(sensor, Equipment)
        self.assertEqual(sensor.type, 'fluxgate')
        self.assertEqual(sensor.description, '3-component analog magnetometer')
        self.assertEqual(sensor.manufacturer, 'LEMI LLC.')
        self.assertIsNone(sensor.vendor)
        self.assertEqual(sensor.model, 'LEMI-039')
        self.assertEqual(sensor.serial_number, 110)

    def test_create_aux_sensor(self):
        """Test basic functionality of create_aux_sensor."""
        sensor = create_aux_sensor(self.md.aux[0])
        self.assertIsInstance(sensor, Equipment)
        self.assertEqual(sensor.type, 'data logger')
        self.assertEqual(sensor.description, 'Input voltage')
        self.assertEqual(sensor.manufacturer, 'LEMI LLC.')
        self.assertIsNone(sensor.vendor)
        self.assertEqual(sensor.model, 'LEMI-424 - long-period 32-bit')
        self.assertEqual(sensor.serial_number, '110')

    def test_init_cha(self):
        """Test basic functionality of init_cha."""
        cha = init_cha(self.md.elec[0])
        self.assertIsInstance(cha, Channel)
        self.assertEqual(cha.code, 'LQN')
        self.assertEqual(cha.location_code, '00')
        self.assertEqual(cha.latitude, 34.048)
        self.assertEqual(cha.longitude, -107.128)
        self.assertEqual(cha.elevation, 2201.725)
        self.assertEqual(cha.depth, 0.0)

    def test_create_cha(self):
        """Test basic functionality of create_cha."""
        cha = create_cha(self.md.elec[0], 'elec')
        with open(self.path2stationxml.joinpath('channel.pkl'), 'rb') as fin:
            expected_cha = pickle.load(fin)
        self.assertEqual(cha, expected_cha)

    def test_update_cha_mag(self):
        """Test basic functionality of update_cha."""
        epoch = [{'a': ['E1', 'E2', 'E3', 'E4', 'Hx', 'Hy', 'Hz', 'Ui', 'Te',
                        'Tf', 'Sn', 'Fq', 'Ce']},
                 {'a': (UTCDateTime('2020-09-30T20:54:00.000000Z'),
                        UTCDateTime('2020-09-30T21:11:01.000000Z'))}]
        e_cha_updates = {}
        cha = create_cha(self.md.mag[0], 'mag')
        updated_cha = update_cha(cha, 'Hx', epoch, e_cha_updates)
        with open(self.path2stationxml.joinpath('updated_magnetic_channel.pkl'), 'rb') as fin:
            expected_cha = pickle.load(fin)
        self.assertEqual(updated_cha, expected_cha)

    def test_get_e_cha_updates_all_electric_channels(self):
        """Test basic functionality of get_e_cha_updates."""
        epoch = [{'a': ['E1', 'E2', 'E3', 'E4', 'Hx', 'Hy', 'Hz', 'Ui', 'Te',
                        'Tf', 'Sn', 'Fq', 'Ce']},
                 {'a': (UTCDateTime('2020-09-30T20:54:00.000000Z'),
                        UTCDateTime('2020-09-30T21:11:01.000000Z'))}]
        md_chas_epoch = [x for x in self.md.elec if x.run_id in epoch[0].keys()]
        e_cha_updates = get_e_cha_updates(md_chas_epoch, epoch[0])
        expected = {'E1': {'a': [0.012, None, 1100, None, 78]},
                    'E2': {'a': [0.011, None, 1200, None, 93]},
                    'E3': {'a': [0.013, None, 1040, None, 78]},
                    'E4': {'a': [0.009, None, 1325, None, 93]}}
        self.assertDictEqual(e_cha_updates, expected)

    def test_get_e_cha_updates_missing_electric_channel(self):
        """Test basic functionality of get_e_cha_updates."""
        epoch = [{'a': ['E1', 'E2', 'E3', 'Hx', 'Hy', 'Hz', 'Ui', 'Te',
                        'Tf', 'Sn', 'Fq', 'Ce']},
                 {'a': (UTCDateTime('2020-09-30T20:54:00.000000Z'),
                        UTCDateTime('2020-09-30T21:11:01.000000Z'))}]
        md_chas_epoch = [x for x in self.md.elec if x.run_id in epoch[0].keys()]
        e_cha_updates = get_e_cha_updates(md_chas_epoch, epoch[0])
        expected = {'E1': {'a': [0.012, None, 1100, None, 78]},
                    'E2': {'a': [0.011, None, 1200, None, 93]},
                    'E3': {'a': [0.013, None, 1040, None, 78]},
                    'E4': {}}
        self.assertDictEqual(e_cha_updates, expected)

    def test_update_cha_elec(self):
        """Test basic functionality of update_cha."""
        epoch = [{'a': ['E1', 'E2', 'E3', 'E4', 'Hx', 'Hy', 'Hz', 'Ui', 'Te',
                        'Tf', 'Sn', 'Fq', 'Ce']},
                 {'a': (UTCDateTime('2020-09-30T20:54:00.000000Z'),
                        UTCDateTime('2020-09-30T21:11:01.000000Z'))}]
        md_chas_epoch = [x for x in self.md.elec if x.run_id in epoch[0].keys()]
        e_cha_updates = get_e_cha_updates(md_chas_epoch, epoch[0])
        cha = create_cha(self.md.elec[0], 'elec')
        updated_cha = update_cha(cha, 'E1', epoch, e_cha_updates)
        with open(self.path2stationxml.joinpath('updated_electric_channel.pkl'), 'rb') as fin:
            expected_cha = pickle.load(fin)
        self.assertEqual(updated_cha, expected_cha)

    def test_get_cha_resp_data_type(self):
        """Test basic functionality of get_cha_resp."""
        cha_resp = get_cha_resp('132', 'mag', 'Hx', 1.0)
        self.assertIsInstance(cha_resp.response_stages[0], PolesZerosResponseStage)

    def test_get_cha_resp_soh_type(self):
        """Test basic functionality of get_cha_resp."""
        cha_resp = get_cha_resp('132', 'aux', 'Te', 1.0)
        self.assertIsInstance(cha_resp.response_stages[0], CoefficientsTypeResponseStage)


class TestDetectChangesEpoch(unittest.TestCase):
    """
    Test suite for detect_new_epoch, detect_changes_elec_cha,
    detect_changes_mag_cha, detect_changes_datalogger functions.
    """

    def setUp(self):
        """Set up test fixtures"""
        lemi_data = LemiData(TEST_DIR.joinpath("EM", "TEST5"), OUTPUT_MSEED, OUTPUT_LOG)
        lemi_data.prep_data()
        path2md = TEST_DIR.joinpath("METADATA")
        lemi_md = LemiMetadata(path2md, OUTPUT_XML, lemi_data)
        md_fields = lemi_md.from_field_sheets
        lemi_md.populate_props(md_fields)
        self.md = lemi_md

    def test_detect_changes_elec_cha_epochs_b(self):
        """Test basic functionality of detect_changes_elec_cha."""
        self.md.run[1].comps_rec = 'E1 - E2 - E3 - E4 - Hx - Hy - Hz'
        self.md.run[2].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        self.md.run[3].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        cha = self.md.init_cha_props('elec', 'b')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'c')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'd')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha_ports = ['E1', 'E3', 'E2', 'E4', 'E1', 'E3', 'E2', 'E4', 'E1', 'E3', 'E2', 'E4']
        for i in range(12):
            self.md.elec[i+4].cha_port = cha_ports[i]
        comps = ['Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey']
        for i in range(12):
            self.md.elec[i+4].comp = comps[i]
        locs = ['00', '00', '01', '01', '00', '00', '01', '01', '00', '00', '01', '01']
        for i in range(12):
            self.md.elec[i+4].loc_code = locs[i]
        self.assertSetEqual(detect_changes_elec_cha(self.md), {'b'})

    def test_detect_changes_elec_cha_epochs_c_d(self):
        """Test basic functionality of detect_changes_elec_cha."""
        self.md.run[1].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        self.md.run[2].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        self.md.run[3].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        cha = self.md.init_cha_props('elec', 'b')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'c')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'd')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha_ports = ['E1', 'E2', 'E3', 'E4', 'E1', 'E3', 'E2', 'E4', 'E1', 'E2', 'E3', 'E4']
        for i in range(12):
            self.md.elec[i+4].cha_port = cha_ports[i]
        comps = ['Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey']
        for i in range(12):
            self.md.elec[i+4].comp = comps[i]
        locs = ['00', '00', '01', '01', '00', '00', '01', '01', '00', '00', '01', '01']
        for i in range(12):
            self.md.elec[i+4].loc_code = locs[i]
        self.assertSetEqual(detect_changes_elec_cha(self.md), {'c', 'd'})

    def test_detect_changes_elec_cha_no_epoch(self):
        """Test basic functionality of detect_changes_elec_cha."""
        self.md.run[1].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        self.md.run[2].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        self.md.run[3].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        cha = self.md.init_cha_props('elec', 'b')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'c')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'd')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha_ports = ['E1', 'E2', 'E3', 'E4', 'E1', 'E2', 'E3', 'E4', 'E1', 'E2', 'E3', 'E4']
        for i in range(12):
            self.md.elec[i+4].cha_port = cha_ports[i]
        comps = ['Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey']
        for i in range(12):
            self.md.elec[i+4].comp = comps[i]
        locs = ['00', '00', '01', '01', '00', '00', '01', '01', '00', '00', '01', '01']
        for i in range(12):
            self.md.elec[i+4].loc_code = locs[i]
        detect_changes_elec_cha(self.md)
        self.assertSetEqual(detect_changes_elec_cha(self.md), set())

    def test_detect_changes_datalogger_one_epoch(self):
        """Test basic functionality of detect_changes_datalogger."""
        self.md.run[1].datalogger_sn = 110
        self.md.run[2].datalogger_sn = 110
        self.md.run[3].datalogger_sn = 110
        self.assertSetEqual(detect_changes_datalogger(self.md), set())

    def test_detect_changes_datalogger_epochs_b(self):
        """Test basic functionality of detect_changes_datalogger."""
        self.md.run[1].datalogger_sn = 111
        self.md.run[2].datalogger_sn = 111
        self.md.run[3].datalogger_sn = 111
        self.assertSetEqual(detect_changes_datalogger(self.md), {'b'})

    def test_detect_changes_datalogger_epochs_b_c_d(self):
        """Test basic functionality of detect_changes_datalogger."""
        self.md.run[1].datalogger_sn = 111
        self.md.run[2].datalogger_sn = 112
        self.md.run[3].datalogger_sn = 113
        self.assertSetEqual(detect_changes_datalogger(self.md), {'b', 'c', 'd'})

    def test_detect_changes_mag_cha_one_epoch(self):
        """Test basic functionality of detect_changes_mag_cha."""
        cha = self.md.init_cha_props('mag', 'b')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        cha = self.md.init_cha_props('mag', 'c')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        cha = self.md.init_cha_props('mag', 'd')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        self.md.mag[3].fluxgate_sn = 110
        self.md.mag[4].fluxgate_sn = 110
        self.md.mag[5].fluxgate_sn = 110
        self.md.mag[6].fluxgate_sn = 110
        self.md.mag[7].fluxgate_sn = 110
        self.md.mag[8].fluxgate_sn = 110
        self.md.mag[9].fluxgate_sn = 110
        self.md.mag[10].fluxgate_sn = 110
        self.md.mag[11].fluxgate_sn = 110
        self.assertSetEqual(detect_changes_mag_cha(self.md), set())

    def test_detect_changes_mag_cha_epochs_c(self):
        """Test basic functionality of detect_changes_mag_cha."""
        cha = self.md.init_cha_props('mag', 'b')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        cha = self.md.init_cha_props('mag', 'c')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        cha = self.md.init_cha_props('mag', 'd')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        self.md.mag[3].fluxgate_sn = 110
        self.md.mag[4].fluxgate_sn = 110
        self.md.mag[5].fluxgate_sn = 110
        self.md.mag[6].fluxgate_sn = 111
        self.md.mag[7].fluxgate_sn = 111
        self.md.mag[8].fluxgate_sn = 111
        self.md.mag[9].fluxgate_sn = 111
        self.md.mag[10].fluxgate_sn = 111
        self.md.mag[11].fluxgate_sn = 111
        self.assertSetEqual(detect_changes_mag_cha(self.md), {'c'})

    def test_detect_new_epoch(self):
        """Test basic functionality of detect_new_epoch."""
        self.md.run[1].comps_rec = 'E1 - E2 - E3 - E4 - Hx - Hy - Hz'
        self.md.run[2].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        self.md.run[3].comps_rec = 'E1 - E2 - Hx - Hy - Hz'
        self.md.run[1].datalogger_sn = 110
        self.md.run[2].datalogger_sn = 111
        self.md.run[3].datalogger_sn = 111
        cha = self.md.init_cha_props('elec', 'b')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'c')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha = self.md.init_cha_props('elec', 'd')
        self.md.elec.extend([self.md.update_e_cha(cha, f'E{ind+1}') for ind in range(4)])
        cha_ports = ['E1', 'E2', 'E3', 'E4', 'E1', 'E3', 'E2', 'E4', 'E1', 'E3', 'E2', 'E4']
        for i in range(12):
            self.md.elec[i+4].cha_port = cha_ports[i]
        comps = ['Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey', 'Ex', 'Ey']
        for i in range(12):
            self.md.elec[i+4].comp = comps[i]
        locs = ['00', '00', '01', '01', '00', '00', '01', '01', '00', '00', '01', '01']
        for i in range(12):
            self.md.elec[i+4].loc_code = locs[i]
        cha = self.md.init_cha_props('mag', 'b')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        cha = self.md.init_cha_props('mag', 'c')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        cha = self.md.init_cha_props('mag', 'd')
        self.md.mag.extend([self.md.update_comp_cha_name(cha, CHA_TYPES['mag'][i])
                            for i in range(3)])
        self.md.mag[3].fluxgate_sn = 110
        self.md.mag[4].fluxgate_sn = 110
        self.md.mag[5].fluxgate_sn = 110
        self.md.mag[6].fluxgate_sn = 111
        self.md.mag[7].fluxgate_sn = 111
        self.md.mag[8].fluxgate_sn = 111
        self.md.mag[9].fluxgate_sn = 111
        self.md.mag[10].fluxgate_sn = 111
        self.md.mag[11].fluxgate_sn = 111
        expected_epochs = [[{'a': ['E1', 'E2', 'E3', 'E4', 'Hx', 'Hy', 'Hz',
                                   'Ui', 'Te', 'Tf', 'Sn', 'Fq', 'Ce'],
                             'b': ['E1', 'E2', 'E3', 'E4', 'Hx', 'Hy', 'Hz',
                                   'Ui', 'Te', 'Tf', 'Sn', 'Fq', 'Ce']},
                            {'a': (UTCDateTime('2020-09-30T20:54:00.000000Z'),
                                   UTCDateTime('2020-09-30T21:11:01.000000Z')),
                             'b': (UTCDateTime('2020-09-30T21:12:00.000000Z'),
                                   UTCDateTime('2020-09-30T21:13:45.000000Z'))}],
                           [{'c': ['E1', 'E3', 'Hx', 'Hy', 'Hz', 'Ui', 'Te',
                                   'Tf', 'Sn', 'Fq', 'Ce'],
                             'd': ['E1', 'E3', 'Hx', 'Hy', 'Hz', 'Ui', 'Te',
                                   'Tf', 'Sn', 'Fq', 'Ce']},
                            {'c': (UTCDateTime('2020-09-30T23:47:00.000000Z'),
                                   UTCDateTime('2020-10-01T00:39:54.000000Z')),
                             'd': (UTCDateTime('2020-10-01T10:53:00.000000Z'),
                                   UTCDateTime('2020-10-01T11:27:05.000000Z'))}]]
        self.assertListEqual(detect_new_epoch(self.md), expected_epochs)
