# -*- coding: utf-8 -*-

"""Tests for `utils` module."""

import unittest

from obspy import UTCDateTime

from lemi2seed.logging import setup_logger
from lemi2seed.utils import (convert_coord, convert_time, str2list,
                             check_email_format, check_inst_specs, check_sn,
                             is_empty, eval_loc_code, get_e_loc, get_e_ids,
                             get_run_list, get_run_num)

SCR_DIR = "lemi2seed.utils"

# Set up logging
logger = setup_logger(SCR_DIR)


class TestConvertCoord(unittest.TestCase):
    """Test suite for convert_coord."""

    def test_convert_coord_positive_lat(self):
        """Test basic functionality of convert_coord."""
        coordinate = 3404.83926
        hemisphere = 'N'
        self.assertEqual(convert_coord(coordinate, hemisphere),
                         34.0483926)

    def test_convert_coord_negative_lat(self):
        """Test basic functionality of convert_coord."""
        coordinate = 2456.43250
        hemisphere = 'S'
        self.assertEqual(convert_coord(coordinate, hemisphere),
                         -24.564325)

    def test_convert_coord_positive_lon(self):
        """Test basic functionality of convert_coord."""
        coordinate = 4303.74563
        hemisphere = 'E'
        self.assertEqual(convert_coord(coordinate, hemisphere),
                         43.0374563)

    def test_convert_coord_negative_lon(self):
        """Test basic functionality of convert_coord."""
        coordinate = 1071.84450
        hemisphere = 'W'
        self.assertEqual(convert_coord(coordinate, hemisphere),
                         -10.718445)

    def test_convert_coord_erroneous_hemisphere(self):
        """Test basic functionality of convert_coord."""
        coordinate = 1071.84450
        hemisphere = 'B'
        self.assertEqual(convert_coord(coordinate, hemisphere), None)

    def test_convert_coord_erroneous_type(self):
        """Test basic functionality of convert_coord."""
        coordinate = '1071.84450'
        hemisphere = 'B'
        self.assertEqual(convert_coord(coordinate, hemisphere), None)


class TestConvertTime(unittest.TestCase):
    """Test suite for convert_time."""

    def test_convert_time(self):
        """Test basic functionality of convert_time."""
        time_stamp = '2020 09 30 20 54 00'
        self.assertEqual(convert_time(time_stamp),
                         UTCDateTime('2020-09-30T20:54:00.000000Z'))

    def test_convert_time_erroneous_time(self):
        """Test basic functionality of convert_time."""
        time_stamp = '2020 09 30 20 74 00'
        self.assertEqual(convert_time(time_stamp), None)

    def test_convert_time_erroneous_type(self):
        """Test basic functionality of convert_time."""
        time_stamp = '2020 09 30 20 74 00a'
        self.assertEqual(convert_time(time_stamp), None)


class TestStr2list(unittest.TestCase):
    """Test suite for str2list."""

    def test_str2list_with_space(self):
        """Test basic functionality of str2list."""
        str_input = 'a, b, c, d, e'
        self.assertEqual(str2list(str_input), ['a', 'b', 'c', 'd', 'e'])

    def test_str2list_no_space(self):
        """Test basic functionality of str2list."""
        str_input = 'a,b,c,d,e'
        self.assertEqual(str2list(str_input), ['a', 'b', 'c', 'd', 'e'])


class TestCheckEmailformat(unittest.TestCase):
    """Test suite for check_email_format."""

    def test_check_email_format_valid_email(self):
        """Test basic functionality of check_email_format."""
        email = 'm@passcal.nmt.edu'
        self.assertTrue(check_email_format(email))

    def test_check_email_format_erroneous_type(self):
        """Test basic functionality of check_email_format."""
        email = 12
        with self.assertLogs(logger, level='ERROR') as cmd:
            check_email_format(email)
        msg = "The provided email '{}' should be a string.".format(email)
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])

    def test_check_email_format_erroneous_email(self):
        """Test basic functionality of check_email_format."""
        email = 'm_passcal.nmt.edu'
        with self.assertLogs(logger, level='ERROR') as cmd:
            check_email_format(email)
        msg = ("Invalid email. The provided email '{}' does not meet minimum "
               "formatting requirements: account@domain.".format(email))
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])


class TestCheckInstSpecs(unittest.TestCase):
    """Test suite for check_inst_specs."""

    def test_check_inst_specs_default_value(self):
        """Test basic functionality of check_inst_specs."""
        specs = 'LEMI-039'
        self.assertTrue(check_inst_specs(specs, 'fluxgate', ''))

    def test_check_inst_specs_valid_user_input(self):
        """Test basic functionality of check_inst_specs."""
        specs = 'Manufacturer: a - Model: b - Type: c'
        self.assertTrue(check_inst_specs(specs, 'fluxgate', ''))

    def test_check_inst_specs_no_type(self):
        """Test basic functionality of check_inst_specs."""
        specs = 'Manufacturer: a - Model: b - Type: '
        self.assertTrue(check_inst_specs(specs, 'fluxgate', ''))

    def test_check_inst_specs_no_manufacturer(self):
        """Test basic functionality of check_inst_specs."""
        specs = 'Manufacturer:  - Model: b - Type: c'
        with self.assertLogs(logger, level='ERROR') as cmd:
            check_inst_specs(specs, 'fluxgate', '')
        msg = "Please provide fluxgate manufacturer. Required metadata field."
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])

    def test_check_inst_specs_no_model(self):
        """Test basic functionality of check_inst_specs."""
        specs = 'Manufacturer: a - Model:  - Type: c'
        with self.assertLogs(logger, level='ERROR') as cmd:
            check_inst_specs(specs, 'fluxgate', '')
        msg = "Please provide fluxgate model. Required metadata field."
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])

    def test_check_inst_specs_none_specs(self):
        """Test basic functionality of check_inst_specs."""
        specs = None
        with self.assertLogs(logger, level='ERROR') as cmd:
            check_inst_specs(specs, 'fluxgate', '')
        msg = "Please provide fluxgate specs for. Required metadata field."
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])

    def test_check_inst_specs_erroneous_specs(self):
        """Test basic functionality of check_inst_specs."""
        specs = 'Other'
        with self.assertLogs(logger, level='ERROR') as cmd:
            check_inst_specs(specs, 'fluxgate', '')
        msg = "Please provide fluxgate specs for. Required metadata field."
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])


class TestCheckSN(unittest.TestCase):
    """Test suite for check_sn."""

    def test_check_sn_no_input(self):
        """Test basic functionality of check_sn."""
        sn = None
        with self.assertLogs(logger, level='ERROR') as cmd:
            valid = check_sn('data logger', sn)
        msg = "Please provide a serial number for the data logger!"
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])
        self.assertFalse(valid)

    def test_check_sn_invalid_input_data_logger(self):
        """Test basic functionality of check_sn."""
        sn = 1234
        with self.assertLogs(logger, level='ERROR') as cmd:
            valid = check_sn('data logger', sn)
        msg = "The serial number of the data logger should be 3 digits long."
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])
        self.assertFalse(valid)

    def test_check_sn_mismatch_between_user_and_data_inputs(self):
        """Test basic functionality of check_sn."""
        sn = 110
        data_input = '132'
        with self.assertLogs(logger, level='ERROR') as cmd:
            valid = check_sn('data logger', sn, data_input)
        msg = ("The serial number of the data logger differs from the one "
               "automatically recorded by the logger in the field (110 != 132). "
               "Please correct!")
        self.assertEqual(cmd.output, [":".join(['ERROR', SCR_DIR, msg])])
        self.assertFalse(valid)

    def test_check_sn_match_between_user_and_data_inputs(self):
        """Test basic functionality of check_sn."""
        sn = 110
        data_input = '110'
        self.assertTrue(check_sn("data logger", sn, data_input))

    def test_check_sn_valid_input_data_logger(self):
        """Test basic functionality of check_sn."""
        sn = 110
        self.assertTrue(check_sn("data logger", sn))

    def test_check_sn_valid_input_fluxgate(self):
        """Test basic functionality of check_sn."""
        sn = 110
        self.assertTrue(check_sn("fluxgate", sn))

    def test_check_sn_valid_input_electrode(self):
        """Test basic functionality of check_sn."""
        sn = 1103455
        self.assertTrue(check_sn("electrode", sn))


class TestIsEmpty(unittest.TestCase):
    """Test suite for is_empty."""

    def test_is_empty_list(self):
        """Test basic functionality of is_empty."""
        val = []
        self.assertTrue(is_empty(val))

    def test_is_empty_dict_set(self):
        """Test basic functionality of is_empty."""
        val = {}
        self.assertTrue(is_empty(val))

    def test_is_empty_none(self):
        """Test basic functionality of is_empty."""
        val = None
        self.assertTrue(is_empty(val))

    def test_is_empty_str(self):
        """Test basic functionality of is_empty."""
        val = ''
        self.assertFalse(is_empty(val))


class TestEvalLocCode(unittest.TestCase):
    """Test suite for eval_loc_code."""

    def test_eval_loc_code_two_electrode_pairs_different_direction(self):
        """Test basic functionality of eval_loc_code."""
        e_infos = {'a': {'E1': 'Ex', 'E2': 'Ey'}, 'b': {'E1': 'Ex', 'E2': 'Ey'}}
        self.assertFalse(eval_loc_code(e_infos))

    def test_eval_loc_code_two_electrode_pairs_same_direction(self):
        """Test basic functionality of eval_loc_code."""
        e_infos = {'a': {'E1': 'Ex', 'E2': 'Ex'}, 'b': {'E1': 'Ex', 'E2': 'Ex'}}
        self.assertTrue(eval_loc_code(e_infos))

    def test_eval_loc_code_more_than_two_electrode_pairs(self):
        """Test basic functionality of eval_loc_code."""
        e_infos = {'a': {'E1': 'Ex', 'E2': 'Ey', 'E3': 'Ex', 'E4': 'Ey'},
                   'b': {'E1': 'Ex', 'E2': 'Ey'}}
        self.assertTrue(eval_loc_code(e_infos))


class TestGetELoc(unittest.TestCase):
    """Test suite for get_e_loc."""

    def test_get_e_loc_two_electrode_pairs_same_direction(self):
        """Test basic functionality of get_e_loc."""
        e_info = {'E1': 'Ex', 'E2': 'Ex'}
        self.assertDictEqual(get_e_loc(e_info), {'E1': '00', 'E2': '01'})

    def test_get_e_loc_more_than_two_electrode_pairs(self):
        """Test basic functionality of get_e_loc."""
        e_info = {'E1': 'Ex', 'E2': 'Ey', 'E3': 'Ex', 'E4': 'Ey'}
        e_loc = {'E1': '00', 'E3': '01', 'E2': '00', 'E4': '01'}
        self.assertDictEqual(get_e_loc(e_info), e_loc)


class TestGetRunListGetEIds(unittest.TestCase):
    """Test suite for get_run_list and get_e_ids."""

    def test_get_run_num(self):
        """Test basic functionality of get_run_num."""
        self.assertEqual(get_run_num('a'), 1)
        self.assertEqual(get_run_num('b'), 2)
        self.assertEqual(get_run_num('c'), 3)

    def test_get_run_list(self):
        """Test basic functionality of get_run_list."""
        num_runs = 3
        self.assertListEqual(get_run_list(num_runs), ['a', 'b', 'c'])

    def test_get_e_ids(self):
        """Test basic functionality of get_e_ids."""
        num_runs = 2
        e_ids = [('a', 1), ('a', 2), ('a', 3), ('a', 4), ('b', 1), ('b', 2),
                 ('b', 3), ('b', 4)]
        self.assertListEqual(get_e_ids(num_runs), e_ids)
